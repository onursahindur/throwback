//
//  TBActivityLog.m
//  Throwback
//
//  Created by Onur Şahindur on 08/07/16.
//  Copyright © 2016 Throwback Corp. All rights reserved.
//

#import "TBActivityLog.h"

static CGFloat const secondsInMonths    = 2592000;
static CGFloat const secondsInWeek      = 604800;
static CGFloat const secondsInDay       = 86400;
static CGFloat const secondsInHour      = 3600;
static CGFloat const secondsInMin       = 60;

@implementation TBActivityLog

- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if (self)
    {
        _ID = [dict objectForKey:@"_id"];
        _directoryID = [dict objectForKey:@"directoryId"];
        _numberOfPhotos = [[dict objectForKey:@"numberOfPhotos"] integerValue];
        _ownerName = [dict objectForKey:@"ownerName"];
        _ownerPhone = [dict objectForKey:@"ownerPhone"];
        _uploadedTime = [dict objectForKey:@"uploadedTime"];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
        NSDate *uploadDate = [dateFormatter dateFromString:_uploadedTime];
        
        NSTimeInterval distanceBetweenDates = [[NSDate date] timeIntervalSinceDate:uploadDate];
        
        if (((NSInteger)(distanceBetweenDates / secondsInMonths)) != 0)
        {
            _shortWhenAnnotation = [NSString stringWithFormat:NSLocalizedString(@"m", nil), (long)(NSInteger)(distanceBetweenDates / secondsInMonths)];
        }
        else if (((NSInteger)(distanceBetweenDates / secondsInWeek)) != 0)
        {
            _shortWhenAnnotation = [NSString stringWithFormat:NSLocalizedString(@"w", nil), (long)(NSInteger)(distanceBetweenDates / secondsInWeek)];
        }
        else if (((NSInteger)(distanceBetweenDates / secondsInDay)) != 0)
        {
            _shortWhenAnnotation = [NSString stringWithFormat:NSLocalizedString(@"d", nil), (long)(NSInteger)(distanceBetweenDates / secondsInDay)];
        }
        else if (((NSInteger)(distanceBetweenDates / secondsInHour)) != 0)
        {
            _shortWhenAnnotation = [NSString stringWithFormat:NSLocalizedString(@"h", nil), (long)(NSInteger)(distanceBetweenDates / secondsInHour)];
        }
        else if (((NSInteger)(distanceBetweenDates / secondsInMin)) != 0)
        {
            _shortWhenAnnotation = [NSString stringWithFormat:NSLocalizedString(@"min", nil), (long)(NSInteger)(distanceBetweenDates / secondsInMin)];
        }
        else
        {
            _shortWhenAnnotation = [NSString stringWithFormat:NSLocalizedString(@"sec", nil), (long)(NSInteger)distanceBetweenDates];
        }
        
        if ((distanceBetweenDates / secondsInHour) <= 24.0f)
        {
            _downloadActive = YES;
        }
        
        _downloadedTime = [dict objectForKey:@"countryCode"];
        if ([[dict objectForKey:@"type"] isEqualToString:@"uploaded"])
        {
            _logType = TBActivityLogTypeUploaded;
        }
        else if ([[dict objectForKey:@"type"] isEqualToString:@"downloaded"])
        {
            _logType = TBActivityLogTypeDownloaded;
        }
        else if ([[dict objectForKey:@"type"] isEqualToString:@"cancelled"])
        {
            _logType = TBActivityLogTypeCancelled;
        }
        
        _recievers = [NSMutableArray new];
        for (NSDictionary *userDic in [dict objectForKey:@"receivers"])
        {
            [_recievers addObject:[[TBUser alloc] initWithDictionary:userDic]];
        }
        
    }
    return self;
}

@end
