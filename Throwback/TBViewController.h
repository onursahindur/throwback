//
//  TBViewController.h
//  Throwback
//
//  Created by Onur Şahindur on 01/07/16.
//  Copyright © 2016 Throwback Corp. All rights reserved.
//

@interface TBViewController : UIViewController

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end
