//
//  TBUser.m
//  Throwback
//
//  Created by Onur Şahindur on 03/07/16.
//  Copyright © 2016 Throwback Corp. All rights reserved.
//

#import "TBUser.h"

@implementation TBUser

- (id)initWithDictionary:(NSDictionary *)user
{
    self = [super init];
    if (self)
    {
        _userID = [user objectForKey:@"_id"];
        _phoneNumber = [user objectForKey:@"phoneNumber"];
        _userName = [user objectForKey:@"name"];
        _notificationID = [user objectForKey:@"notificationId"];
        _regionType = [user objectForKey:@"regionType"];
        _countryCode = [user objectForKey:@"countryCode"];
        _downloadList = [user objectForKey:@"downloadList"];
        _active = [user objectForKey:@"active"];
        _downloaded = [[user objectForKey:@"downloaded"] boolValue];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder
{
    if((self = [super init]))
    {
        _userID = [decoder decodeObjectForKey:@"userId"];
        _phoneNumber = [decoder decodeObjectForKey:@"phoneNumber"];
        _countryCode = [decoder decodeObjectForKey:@"countryCode"];
        _userName = [decoder decodeObjectForKey:@"userName"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
    //Encode properties, other class variables, etc
    [encoder encodeObject:_userID forKey:@"userId"];
    [encoder encodeObject:_phoneNumber forKey:@"phoneNumber"];
    [encoder encodeObject:_countryCode forKey:@"countryCode"];
    [encoder encodeObject:_userName forKey:@"userName"];
}

@end
