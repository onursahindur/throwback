//
//  AlertManager.m
//  Throwback
//
//  Created by Onur Şahindur on 01/07/16.
//  Copyright © 2016 Throwback Corp. All rights reserved.
//

#import "AlertManager.h"

@interface AlertManager ()

@property (nonatomic, copy)     void (^completionHandler)(NSInteger buttonClicked);
@property (nonatomic, strong)   UIAlertController   *alertController;
@property (nonatomic, strong)   UIActionSheet       *actionSheet;

@end

@implementation AlertManager

+ (instancetype)sharedInstance
{
    static AlertManager *manager = nil;
    static dispatch_once_t token;
    
    dispatch_once(&token, ^{
        manager = [AlertManager new];
    });
    
    return manager;
}

+ (void) showAlertWithTitle:(NSString *)title
                    message:(NSString *)message
          cancelButtonTitle:(NSString *)cancelButtonTitle
          otherButtonTitles:(NSArray *)otherButtonTitles
             viewController:(UIViewController *)viewController
          completionHandler:(void (^)(NSInteger buttonClicked))completionHandler
{
    [AlertManager sharedInstance].alertController = [UIAlertController alertControllerWithTitle:title.copy
                                                                                           message:message.copy
                                                                                    preferredStyle:UIAlertControllerStyleAlert];
    if(cancelButtonTitle)
    {
        UIAlertAction *action = [UIAlertAction actionWithTitle:cancelButtonTitle
                                                         style:UIAlertActionStyleCancel
                                                       handler:^(UIAlertAction *action) {
                                                           if(completionHandler)
                                                           {
                                                               completionHandler(0);
                                                           }
                                                       }];
        
        [[AlertManager sharedInstance].alertController addAction:action];
    }
    
    for(NSInteger i = 0; i < otherButtonTitles.count; i++)
    {
        UIAlertAction *action = [UIAlertAction actionWithTitle:otherButtonTitles[i]
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction *action) {
                                                           if(completionHandler)
                                                           {
                                                               completionHandler(i+1);
                                                           }
                                                       }];
        
        [[AlertManager sharedInstance].alertController addAction:action];
    }
    
    [viewController presentViewController:[AlertManager sharedInstance].alertController animated:YES completion:nil];
}

+ (void)showErrorAlertWithError:(NSError *)error
              otherButtonTitles:(NSArray *)otherButtonTitles
                 viewController:(UIViewController *)viewController
              completionHandler:(void (^)(NSInteger buttonClicked))completionHandler
{
    
    [self showAlertWithTitle:NSLocalizedString(@"Error", nil) message:error.localizedDescription cancelButtonTitle:NSLocalizedString(@"Done", nil) otherButtonTitles:otherButtonTitles viewController:viewController completionHandler:completionHandler];
}

+ (void)showActionSheetWithTitle:(NSString *)title
                         message:(NSString *)message
               cancelButtonTitle:(NSString *)cancelButtonTitle
               otherButtonTitles:(NSArray *)otherButtonTitles
                  viewController:(UIViewController *)viewController
               completionHandler:(void (^)(NSInteger buttonClicked))completionHandler
{
    
    
    
    
}

+ (void)showTSAlertWithTitle:(NSString *)title
                     message:(NSString *)message
                        type:(TSMessageNotificationType)type
{
    [TSMessage setDefaultViewController:[UIViewController currentViewController]];
    [TSMessage addCustomDesignFromFileWithName:@"ThrowbackMessageDesign.json"];
    [TSMessage showNotificationWithTitle:title
                                subtitle:message
                                    type:type];
}

@end
