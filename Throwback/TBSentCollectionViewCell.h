//
//  TBSentCollectionViewCell.h
//  Throwback
//
//  Created by Onur Şahindur on 01/07/16.
//  Copyright © 2016 Throwback Corp. All rights reserved.
//

@class TBSentCollectionViewCell;
@protocol TBSentCollectionViewCellDelegate <NSObject>

- (void)sentCollectionViewCellDeleteButtonTapped:(TBSentCollectionViewCell *)cell
                                    withActivity:(TBActivityLog *)activity;

@end

@interface TBSentCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIButton   *deleteButton;
@property (weak, nonatomic) IBOutlet UILabel    *sentUserNameLabel;
@property (weak, nonatomic) IBOutlet UILabel    *photoCountLabel;
@property (weak, nonatomic) IBOutlet UILabel    *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel    *statusLabel;
@property (weak, nonatomic) IBOutlet UIImageView *statusImageView;

@property (strong, nonatomic) TBActivityLog *activity;

@property (weak, nonatomic) id<TBSentCollectionViewCellDelegate>delegate;

@end
