//
//  TBWhoAreWeViewController.m
//  Throwback
//
//  Created by Onur Şahindur on 16/07/16.
//  Copyright © 2016 Throwback Corp. All rights reserved.
//

#import "TBWhoAreWeViewController.h"

@interface TBWhoAreWeViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *onurImageView;
@property (weak, nonatomic) IBOutlet UIImageView *ufukImageView;

@end

@implementation TBWhoAreWeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.onurImageView.layer.cornerRadius = self.onurImageView.frame.size.height / 2;
    self.onurImageView.layer.masksToBounds = YES;
    self.onurImageView.layer.borderWidth = 0.5f;
    self.ufukImageView.layer.cornerRadius = self.ufukImageView.frame.size.height / 2;
    self.ufukImageView.layer.masksToBounds = YES;
    self.ufukImageView.layer.borderWidth = 0.5f;
    [AnalyticsManager trackScreenWithName:@"Who are we"];
}

@end
