//
//  UIViewController+Utils.h
//  KanalD_iPad
//
//  Created by Onur Şahindur on 02/06/16.
//  Copyright © 2016 OnurSahindur. All rights reserved.
//

@interface UIViewController (Utils)

+ (UIViewController*) currentViewController;

@end
