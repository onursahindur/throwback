//
//  AnalyticsManager.h
//  KanalD_iPad
//
//  Created by Onur Şahindur on 30.05.2016.
//  Copyright © 2016 OnurSahindur. All rights reserved.
//

// Screens

@interface AnalyticsManager : NSObject

+ (void)trackScreenWithName:(NSString *)screenName;

+ (void)trackEventWithAction:(NSString *)eventAction
           withEventCategory:(NSString *)eventCategory
              withEventLabel:(NSString *)eventLabel;
@end
