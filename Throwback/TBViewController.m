//
//  TBViewController.m
//  Throwback
//
//  Created by Onur Şahindur on 01/07/16.
//  Copyright © 2016 Throwback Corp. All rights reserved.
//

#import "TBViewController.h"

@interface TBViewController ()

@end

@implementation TBViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBar.topItem.title = @"";
    self.navigationController.navigationBar.tintColor = TB_BLUE;
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:TB_BLUE}];
    self.navigationController.navigationBar.translucent = NO;
}

@end
