//
//  AddressbookManager.h
//  Throwback
//
//  Created by Onur Şahindur on 01/07/16.
//  Copyright © 2016 Throwback Corp. All rights reserved.
//

@interface AddressbookManager : NSObject

+ (void)accessGrantedWithCompletionHandler:(void (^)(BOOL granted))completionHandler;

+ (void)getContactNamesWithViewController:(UIViewController *)viewController
                    withCompletionHandler:(void (^)(NSArray *contacts, NSArray *phoneNumbers))completionHandler;

@end
