//
//  NetworkManager.m
//  Throwback
//
//  Created by Onur Şahindur on 07/07/16.
//  Copyright © 2016 Throwback Corp. All rights reserved.
//

static NSString * const kBaseURL                            = @"https://throwbackapp.herokuapp.com/api/v1/";
static NSString * const kCheckVersion                       = @"check-version";
static NSString * const kStatusKey                          = @"status";
static NSString * const kRegisterKey                        = @"register";
static NSString * const kAuthenticateKey                    = @"authenticate";
static NSString * const kUpdateKey                          = @"update";
static NSString * const kUploadFinished                     = @"upload-finished";
static NSString * const kDownloadFinished                   = @"download-finished";
static NSString * const kGetDownloadList                    = @"download-list";
static NSString * const kGetRecentActivities                = @"recent";
static NSString * const kDeleteActivity                     = @"cancel";
static NSString * const kCheckContacts                      = @"check-contacts";
static NSString * const kCheckActiviy                       = @"check-activity";
static NSString * const kLogoutKey                          = @"logout";

static NSString * const kTopSecretKey                       = @"qA7SfWyu9LY9M8G";

#import "NetworkManager.h"
#import "AESCrypt.h"

@interface NetworkManager ()

@end

@implementation NetworkManager

+ (instancetype)sharedInstance
{
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[NetworkManager alloc] initWithBaseURL:[NSURL URLWithString:kBaseURL]];
        ((NetworkManager *)sharedInstance).responseSerializer.acceptableContentTypes = [NSSet setWithArray:@[@"text/html", @"application/json", @"text/json", @"text/javascript", @"text/plain"]];
        ((NetworkManager *)sharedInstance).requestSerializer = [AFJSONRequestSerializer serializer];
        [((NetworkManager *)sharedInstance).requestSerializer setTimeoutInterval:30];
        [((NetworkManager *)sharedInstance).requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [((NetworkManager *)sharedInstance) makeInitializationConfigurations];
        [((NetworkManager *)sharedInstance).reachabilityManager startMonitoring];
        [[NSNotificationCenter defaultCenter] addObserver:sharedInstance selector:@selector(reachabilityDidChange:) name:AFNetworkingReachabilityDidChangeNotification object:nil];
        [((NetworkManager *)sharedInstance) performSelector:@selector(checkReachability) withObject:nil afterDelay:0.1];
        
    });
    return sharedInstance;
}

- (void)makeInitializationConfigurations
{
    self.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringLocalCacheData;
    
}

#pragma mark - Cancel services
- (void)cancelRunningServices
{
    [[self operationQueue] cancelAllOperations];
}

#pragma mark - Reachability
- (void)reachabilityDidChange:(NSNotification *)notification
{
    AFNetworkReachabilityStatus status = [notification.userInfo[AFNetworkingReachabilityNotificationStatusItem] integerValue];
    if (status == AFNetworkReachabilityStatusNotReachable) {
        
        __weak typeof(self) weakSelf = self;
        [AlertManager showAlertWithTitle:nil
                                 message:NSLocalizedString(@"NoConnectionMessage", nil)
                       cancelButtonTitle:NSLocalizedString(@"Close", nil)
                       otherButtonTitles:@[NSLocalizedString(@"Retry", nil)]
                          viewController:[[[UIApplication sharedApplication] delegate] window].rootViewController
                       completionHandler:^(NSInteger buttonClicked) {
                              if (buttonClicked == 1) {
                                  [weakSelf checkReachability];
                              }
                          }
         ];
    }
}

- (void)checkReachability
{
    if (self.reachabilityManager.networkReachabilityStatus == AFNetworkReachabilityStatusNotReachable)
    {
        [self reachabilityDidChange:[[NSNotification alloc] initWithName:AFNetworkingReachabilityDidChangeNotification
                                                                  object:nil
                                                                userInfo:@{AFNetworkingReachabilityNotificationStatusItem : @(self.reachabilityManager.networkReachabilityStatus)}]];
    }
}

#pragma mark - AFNetworking Requests
- (AFHTTPRequestOperation *)GET:(NSString *)URLString
                     parameters:(id)parameters
                        success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                        failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    [self.requestSerializer setValue:[NSString stringWithFormat:@"%@", [Manager sharedInstance].accessToken] forHTTPHeaderField:@"Authorization"];
    return [super GET:URLString
           parameters:parameters
              success:^(AFHTTPRequestOperation *operation, id responseObject) {
                  
                  [self handleGetWithOperation:operation
                                responseObject:responseObject
                                         error:nil
                                 withURLString:URLString
                                withParameters:parameters
                                  successBlock:success
                                  failureBlock:failure];
                  
              }
              failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  
                  [self handleGetWithOperation:operation
                                responseObject:nil
                                         error:error
                                 withURLString:URLString
                                withParameters:parameters
                                  successBlock:success
                                  failureBlock:failure];
              }];
    
}

- (AFHTTPRequestOperation *)POST:(NSString *)URLString
                      parameters:(id)parameters
                         success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                         failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    [self.requestSerializer setValue:[NSString stringWithFormat:@"%@", [Manager sharedInstance].accessToken] forHTTPHeaderField:@"Authorization"];
    return [super POST:URLString
            parameters:parameters
               success:^(AFHTTPRequestOperation *operation, id responseObject) {
                   
                   [self handlePostWithOperation:operation
                                  responseObject:responseObject
                                           error:nil
                                   withURLString:URLString
                                  withParameters:parameters
                                    successBlock:success
                                    failureBlock:failure];
                   
               }
               failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                   
                   [self handlePostWithOperation:operation
                                  responseObject:nil
                                           error:error
                                   withURLString:URLString
                                  withParameters:parameters
                                    successBlock:success
                                    failureBlock:failure];
               }];
    
}

- (void)handleGetWithOperation:(AFHTTPRequestOperation *)operation
                responseObject:(id)responseObject
                         error:(NSError *)error
                 withURLString:URLString
                withParameters:parameters
                  successBlock:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                  failureBlock:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    if(error)
    {
        if (operation.response.statusCode >= 500)
        {
            [[Manager sharedInstance] showMaintanenceAlert];
        }
        else if (operation.response.statusCode == 401)
        {   //unauthorized
            //make authorization
            [self authenticateWithUser:[UserManager loadUser] withSuccessBlock:^(NSString *token) {
                if ([token isEqualToString:@"error"])
                {
                    failure(operation, error);
                }
                else
                {
                    [Manager sharedInstance].accessToken = token;
                    [super GET:URLString parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
                        DebugLog(@"operation success: %@ \n with response: %@", operation, responseObject);
                        success(operation, responseObject);
                    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                        DebugLog(@"Network Error with url : %@ error : %@", [operation.request.URL absoluteString], [error localizedDescription]);
                        failure(operation, error);
                    }];
                }
            } failureBlock:^(NSError *error) {
                failure(nil, error);
            }];
        }
        DebugLog(@"Network Error with url : %@ error : %@", [operation.request.URL absoluteString], [error localizedDescription]);
        failure(operation, error);
    }
    else
    {
        DebugLog(@"operation success:%@", operation);
        DebugLog(@"%@", responseObject);
        success(operation, responseObject);
    }
}

- (void)handlePostWithOperation:(AFHTTPRequestOperation *)operation
                 responseObject:(id)responseObject
                          error:(NSError *)error
                  withURLString:URLString
                 withParameters:parameters
                   successBlock:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                   failureBlock:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    if(error)
    {
        if (operation.response.statusCode >= 500)
        {
            [[Manager sharedInstance] showMaintanenceAlert];
        }
        DebugLog(@"Network Error with url : %@ error : %@", [operation.request.URL absoluteString], [error localizedDescription]);
        failure(operation, error);
    }
    else
    {
        DebugLog(@"operation success:%@", operation);
        DebugLog(@"%@", responseObject);
        success(operation, responseObject);
    }
}

#pragma mark - Throwback methods
- (void)checkVersionWithVersion:(NSString *)version
               withSuccessBlock:(void (^)(VersionType))successBlock
                   failureBlock:(FailureBlock)failureBlock
{
    [self GET:[NSString stringWithFormat:@"%@/%@/%@",kCheckVersion, @"ios", version] parameters:nil success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        successBlock((VersionType)[[responseObject objectForKey:@"status"] integerValue]);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        failureBlock(error);
    }];
}

- (void)checkServerStateWithSuccessBlock:(void (^)(BOOL))successBlock
                            failureBlock:(FailureBlock)failureBlock
{
    [self GET:kStatusKey parameters:nil success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        successBlock(YES);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        failureBlock(error);
    }];
}

- (void)registerUserWithUser:(TBUser *)user
            withSuccessBlock:(void (^)(TBUser *))successBlock
                failureBlock:(FailureBlock)failureBlock
{
    NSMutableDictionary *newUser = [NSMutableDictionary dictionary];
    [newUser setValue:user.userName forKey:@"name"];
    [newUser setValue:user.notificationID forKey:@"notificationId"];
    [newUser setValue:user.phoneNumber forKey:@"phoneNumber"];
    [newUser setValue:user.regionType forKey:@"regionType"];
    [newUser setValue:user.countryCode forKey:@"countryCode"];
    [newUser setValue:@"ios" forKey:@"platform"];
    NSMutableDictionary *props = [NSMutableDictionary dictionary];
    [props setValue:newUser forKey:@"user"];
    
    [self POST:kRegisterKey parameters:props success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        TBUser *returnedUser = [[TBUser alloc] initWithDictionary:responseObject];
        successBlock(returnedUser);
        
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        failureBlock(error);
    }];
}

- (void)authenticateWithUser:(TBUser *)user
            withSuccessBlock:(void (^)(NSString *))successBlock
                failureBlock:(FailureBlock)failureBlock
{
    //NSString *encryptedData = [AESCrypt encrypt:user.phoneNumber password:kTopSecretKey];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:user.phoneNumber forKey:@"phoneNumber"];
    [params setValue:@"ios" forKey:@"platform"];
    [params setValue:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"] forKey:@"appVersion"];
    [self POST:kAuthenticateKey parameters:params success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        if ([[responseObject objectForKey:@"success"] boolValue])
        {
            successBlock([responseObject objectForKey:@"token"]);
        }
        else
        {
            successBlock(@"error");
        }
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        failureBlock(error);
    }];
}

- (void)updateUser:(TBUser *)user
  withSuccessBlock:(void (^)(BOOL))successBlock
      failureBlock:(FailureBlock)failureBlock
{
    NSMutableDictionary *uUser = [NSMutableDictionary dictionary];
    [uUser setValue:user.userName forKey:@"name"];
    NSMutableDictionary *props = [NSMutableDictionary dictionary];
    [props setValue:uUser forKey:@"user"];
    
    [self POST:kUpdateKey parameters:props success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        successBlock(YES);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        failureBlock(error);
    }];
    
}

- (void)sendUploadFinishedFlagWithActivityLog:(TBActivityLog *)activity
                             withSuccessBlock:(void (^)(BOOL))successBlock
                                 failureBlock:(FailureBlock)failureBlock
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setValue:activity.directoryID forKey:@"directoryId"];
    [dict setValue:activity.ownerName forKey:@"ownerName"];
    [dict setValue:activity.ownerPhone forKey:@"ownerPhone"];
    NSMutableDictionary *recievers = [NSMutableDictionary dictionary];
    for (TBUser *user in activity.recievers)
    {
        [recievers setValue:user.userName forKey:user.phoneNumber];
    }
    [dict setValue:recievers forKey:@"receivers"];
    [dict setValue:[NSNumber numberWithInteger:activity.numberOfPhotos] forKey:@"numberOfPhotos"];
    NSMutableDictionary *props = [NSMutableDictionary dictionary];
    [props setValue:dict forKey:@"activityObject"];
    
    [self POST:kUploadFinished parameters:props success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        successBlock(YES);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        failureBlock(error);
    }];
}

- (void)sendDownloadFinishedFlagWithActivityLog:(TBActivityLog *)activity
                               withSuccessBlock:(void (^)(BOOL))successBlock
                                   failureBlock:(FailureBlock)failureBlock
{
    [self GET:[NSString stringWithFormat:@"%@/%@", kDownloadFinished, activity.ID] parameters:nil success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        successBlock(YES);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        failureBlock(error);
    }];
}

- (void)getDownloadListWithUser:(TBUser *)user
                        withSuccessBlock:(void (^)(NSArray *))successBlock
                            failureBlock:(FailureBlock)failureBlock
{
    [self GET:[NSString stringWithFormat:@"%@", kGetDownloadList] parameters:nil success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        
        NSMutableArray *activities = [NSMutableArray new];
        for (NSDictionary *dict in responseObject)
        {
            TBActivityLog *activity = [[TBActivityLog alloc] initWithDictionary:dict];
            if (activity.downloadActive) [activities addObject:activity];
        }
        successBlock([[[activities reverseObjectEnumerator] allObjects] mutableCopy]);
        
        
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        failureBlock(error);
    }];
}

- (void)getRecentActivitiesWithUser:(TBUser *)user
                   withSuccessBlock:(void (^)(NSArray *))successBlock
                       failureBlock:(FailureBlock)failureBlock
{
    [self GET:[NSString stringWithFormat:@"%@", kGetRecentActivities] parameters:nil success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        
        NSMutableArray *activities = [NSMutableArray new];
        for (NSDictionary *dict in responseObject)
        {
            [activities addObject:[[TBActivityLog alloc] initWithDictionary:dict]];
        }
        successBlock(activities);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        failureBlock(error);
    }];
}

- (void)deleteSelectedActivityWithActivity:(TBActivityLog *)activity
                          withSuccessBlock:(void (^)(BOOL))successBlock
                              failureBlock:(FailureBlock)failureBlock
{
    [self GET:[NSString stringWithFormat:@"%@/%@", kDeleteActivity, activity.ID] parameters:nil success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        successBlock(YES);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        failureBlock(error);
    }];
}

- (void)userCheckContactsWithContacts:(NSMutableArray *)contactPhones
                     withSuccessBlock:(void (^)(NSArray *))successBlock
                         failureBlock:(FailureBlock)failureBlock
{
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:contactPhones forKey:@"contacts"];
    [self POST:kCheckContacts parameters:params success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        NSMutableArray *tbUsers = [NSMutableArray new];
        for (NSDictionary *dict in responseObject)
        {
            [tbUsers addObject:[[TBUser alloc] initWithDictionary:dict]];
        }
        successBlock(tbUsers);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        failureBlock(error);
    }];
}

- (void)checkActivityStatus:(NSString *)activityID
           withSuccessBlock:(void (^)(NSString *))successBlock
               failureBlock:(FailureBlock)failureBlock
{
    [self GET:[NSString stringWithFormat:@"%@/%@", kCheckActiviy, activityID] parameters:nil success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        successBlock([responseObject objectForKey:@"type"]);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        failureBlock(error);
    }];
}

- (void)logoutWithSuccessBlock:(void (^)(BOOL))successBlock
                  failureBlock:(FailureBlock)failureBlock
{
    [self GET:kLogoutKey parameters:nil success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        successBlock(YES);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        failureBlock(error);
    }];
}

@end
