//
//  AddressbookManager.m
//  Throwback
//
//  Created by Onur Şahindur on 01/07/16.
//  Copyright © 2016 Throwback Corp. All rights reserved.
//

#import "AddressbookManager.h"
#import <APAddressBook/APAddressBook.h>
#import <APAddressBook/APContact.h>

static NSInteger const kPhoneNumberCount = 10;

@implementation AddressbookManager

+ (void)accessGrantedWithCompletionHandler:(void (^)(BOOL granted))completionHandler
{
    APAddressBook *addressBook = [[APAddressBook alloc] init];
    [addressBook requestAccess:^(BOOL granted, NSError *error)
    {
        completionHandler(granted);
    }];
}

+ (void)getContactNamesWithViewController:(UIViewController *)viewController
                    withCompletionHandler:(void (^)(NSArray *contacts, NSArray *phoneNumbers))completionHandler

{
    APAddressBook *addressBook = [[APAddressBook alloc] init];
    addressBook.fieldsMask = APContactFieldName | APContactFieldPhonesOnly;
    addressBook.sortDescriptors = @[
                                    [NSSortDescriptor sortDescriptorWithKey:@"name.firstName" ascending:YES],
                                    [NSSortDescriptor sortDescriptorWithKey:@"name.lastName" ascending:YES]
                                    ];
    [addressBook loadContacts:^(NSArray <APContact *> *contacts, NSError *error)
    {
        // hide activity
        if (!error)
        {
            NSMutableArray *contactsString = [NSMutableArray new];
            NSMutableArray *phoneNumbersString = [NSMutableArray new];
            for (APContact *contact in contacts)
            {
                if (contact.phones && contact.phones.count > 0)
                {
                    for (APPhone *phone in contact.phones)
                    {
                        NSString *contactName = [NSString stringWithFormat:@"%@-%@-%@", contact.name.firstName, contact.name.middleName, contact.name.lastName];
                        contactName = [contactName stringByReplacingOccurrencesOfString:@"(null)" withString:@""];
                        contactName = [contactName stringByReplacingOccurrencesOfString:@"--" withString:@" "];
                        contactName = [contactName stringByReplacingOccurrencesOfString:@"-" withString:@" "];
                        contactName = [contactName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                        if ([contactName isEqualToString:@""]) contactName = NSLocalizedString(@"noName", nil);
                        [contactsString addObject:contactName];
                        
                        NSString *modifiedPN = [[phone.number componentsSeparatedByCharactersInSet:
                                                 [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                                                componentsJoinedByString:@""];
                        if ([modifiedPN length] == kPhoneNumberCount)
                        {
                            modifiedPN = [NSString stringWithFormat:@"%@%@", [UserManager loadUser].countryCode, modifiedPN];
                        }
                        else if ([modifiedPN length] == kPhoneNumberCount + 1 && [[modifiedPN substringToIndex:1] isEqualToString:@"0"])
                        {
                            modifiedPN = [NSString stringWithFormat:@"%@%@", [UserManager loadUser].countryCode, [modifiedPN substringFromIndex:1]];
                        }
                        else if ([modifiedPN length] == kPhoneNumberCount + 1)
                        {
                            modifiedPN = [NSString stringWithFormat:@"+%@%@", [modifiedPN substringToIndex:modifiedPN.length - 10], [modifiedPN substringFromIndex:[modifiedPN length] - kPhoneNumberCount]];
                        }
                        else if ([modifiedPN length] == kPhoneNumberCount + 2 && [[modifiedPN substringToIndex:2] isEqualToString:[[UserManager loadUser].countryCode stringByReplacingOccurrencesOfString:@"+" withString:@""]])
                        {
                            modifiedPN = [NSString stringWithFormat:@"%@%@", [UserManager loadUser].countryCode, [modifiedPN substringFromIndex:2]];
                        }
                        else if ([modifiedPN length] >= kPhoneNumberCount + 2)
                        {
                            modifiedPN = [NSString stringWithFormat:@"%@%@", [[modifiedPN substringToIndex:modifiedPN.length - kPhoneNumberCount] stringByReplacingOccurrencesOfString:@"00" withString:@"+"], [modifiedPN substringFromIndex:[modifiedPN length] - kPhoneNumberCount]];
                        }
                        [phoneNumbersString addObject:modifiedPN];
                    }
                }
            }
            // do something with contacts array
            completionHandler(contactsString, phoneNumbersString);
        }
        else
        {
            // show error
        }
    }];
}

@end
