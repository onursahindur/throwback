//
//  TBUsersDownloadedViewController.h
//  Throwback
//
//  Created by Onur Şahindur on 15/07/16.
//  Copyright © 2016 Throwback Corp. All rights reserved.
//

#import "TBViewController.h"

@interface TBUsersDownloadedViewController : TBViewController

@property (nonatomic, strong) NSArray *recieverArray;

@end
