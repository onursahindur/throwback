//
//  AlertManager.h
//  Throwback
//
//  Created by Onur Şahindur on 01/07/16.
//  Copyright © 2016 Throwback Corp. All rights reserved.
//

#import <TSMessages/TSMessage.h>
#import <TSMessages/TSMessageView.h>
#import "UIViewController+Utils.h"

@interface AlertManager : NSObject

+ (void)showAlertWithTitle:(NSString *)title
                   message:(NSString *)message
         cancelButtonTitle:(NSString *)cancelButtonTitle
         otherButtonTitles:(NSArray *)otherButtonTitles
            viewController:(UIViewController *)viewController
         completionHandler:(void (^)(NSInteger buttonClicked))completionHandler;

+ (void)showErrorAlertWithError:(NSError *)error
         otherButtonTitles:(NSArray *)otherButtonTitles
            viewController:(UIViewController *)viewController
         completionHandler:(void (^)(NSInteger buttonClicked))completionHandler;

+ (void)showActionSheetWithTitle:(NSString *)title
                         message:(NSString *)message
               cancelButtonTitle:(NSString *)cancelButtonTitle
               otherButtonTitles:(NSArray *)otherButtonTitles
                  viewController:(UIViewController *)viewController
               completionHandler:(void (^)(NSInteger buttonClicked))completionHandler;

+ (void)showTSAlertWithTitle:(NSString *)title
                     message:(NSString *)message
                        type:(TSMessageNotificationType)type;


@end
