//
//  Manager.h
//  Throwback
//
//  Created by Onur Şahindur on 01/07/16.
//  Copyright © 2016 Throwback Corp. All rights reserved.
//

#import "TBActivityLog.h"
@interface Manager : NSObject

typedef void (^FailureBlock)(NSError *error);

+ (instancetype)sharedInstance;

- (UIViewController *)buildViewControllerFromStoryBoard:(NSString *)storyboardName
                                   withViewControllerId:(NSString *)vcID;

- (void)showMaintanenceAlert;

- (void)addBlurEffectToView:(UIView *)view;

- (void)removeBlurEffect;

- (void)registerForRemoteNotificationsWithSuccessBlock:(void (^)(BOOL))successBlock;

- (UIViewController*)topMostController;

@property (strong, nonatomic) NSString          *accessToken;
@property (strong, nonatomic) NSString          *currentUserPushId;
@property (strong, nonatomic) NSString          *currentUserPushToken;
@property (strong, nonatomic) TBActivityLog     *activityLogFromNotification;
@property (strong, nonatomic) NSMutableArray    *tbContactArray;

@end
