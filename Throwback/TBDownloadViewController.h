//
//  TBDownloadViewController.h
//  Throwback
//
//  Created by Onur Şahindur on 09/07/16.
//  Copyright © 2016 Throwback Corp. All rights reserved.
//

#import "TBViewController.h"

@class TBActivityLog;
@interface TBDownloadViewController : TBViewController

@property (strong, nonatomic) TBActivityLog *activityFromNotification;

- (void)askDownloadRequest;

@end
