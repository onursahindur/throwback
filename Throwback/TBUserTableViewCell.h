//
//  TBUserTableViewCell.h
//  Throwback
//
//  Created by Onur Şahindur on 15/07/16.
//  Copyright © 2016 Throwback Corp. All rights reserved.
//

@interface TBUserTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel        *userNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView    *statusImageView;

@end
