//
//  TBCreateUserViewController.h
//  Throwback
//
//  Created by Onur Şahindur on 03/07/16.
//  Copyright © 2016 Throwback Corp. All rights reserved.
//

#import "TBViewController.h"

@interface TBCreateUserViewController : TBViewController

@property (nonatomic, strong) DGTSession *session;

@end
