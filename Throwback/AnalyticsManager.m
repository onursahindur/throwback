//
//  KDIAnalyticsManager.m
//  KanalD_iPad
//
//  Created by Onur Şahindur on 30.05.2016.
//  Copyright © 2016 OnurSahindur. All rights reserved.
//

#import "AnalyticsManager.h"
#import <Google/Analytics.h>

@implementation AnalyticsManager

+ (void)trackScreenWithName:(NSString *)screenName
{
    id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName
           value:screenName];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

+ (void)trackEventWithAction:(NSString *)eventAction
           withEventCategory:(NSString *)eventCategory
              withEventLabel:(NSString *)eventLabel
{
    id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:eventCategory
                                                          action:eventAction
                                                           label:eventLabel
                                                           value:nil] build]];
}

@end
