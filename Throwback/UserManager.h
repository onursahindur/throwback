//
//  UserManager.h
//  Throwback
//
//  Created by Onur Şahindur on 03/07/16.
//  Copyright © 2016 Throwback Corp. All rights reserved.
//

#import "TBUser.h"
@interface UserManager : NSObject

+ (BOOL)saveUser:(TBUser *)user;
+ (TBUser *)loadUser;
+ (BOOL)clearSavedUser;

@end
