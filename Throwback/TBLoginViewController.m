//
//  TBLoginViewController.m
//  Throwback
//
//  Created by Onur Şahindur on 03/07/16.
//  Copyright © 2016 Throwback Corp. All rights reserved.
//

#import "TBLoginViewController.h"
#import "TBCreateUserViewController.h"
#import "MBCircularProgressBarView.h"
#import "TBSorryViewController.h"

@interface TBLoginViewController ()

@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (weak, nonatomic) IBOutlet UILabel *letsTBLabel;
@property (weak, nonatomic) IBOutlet UIImageView *tbLogo;
@property (weak, nonatomic) IBOutlet UIButton *tbButton;
@property (weak, nonatomic) IBOutlet MBCircularProgressBarView *circularProgressBarView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tbVeriticalConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *circularVerticalConstraint;

@property (assign, nonatomic) BOOL finished;
@property (assign, nonatomic) BOOL asked;
@property (assign, nonatomic) BOOL versionPrompted;

@end

@implementation TBLoginViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initUI];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (self.versionPrompted)
    {
        [self animateLogo];
    }
    else
    {
        [self versionCheck];
    }
}

- (void)versionCheck
{
    __weak __typeof(self)weakSelf = self;
    [[NetworkManager sharedInstance] checkVersionWithVersion:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"] withSuccessBlock:^(VersionType type)
     {
         __strong __typeof(weakSelf)strongSelf = weakSelf;
         strongSelf.versionPrompted = YES;
         if (type == VersionTypeNone)
         {
             [strongSelf animateLogo];
         }
         else
         {
             [[Manager sharedInstance] addBlurEffectToView:strongSelf.view];
             NSString *title;
             NSArray *otherButtons;
             if (type == VersionTypeUpdate)
             {
                 title = NSLocalizedString(@"UpdateInfo", nil);
                 otherButtons = @[NSLocalizedString(@"Cancel", nil)];
             }
             else
             {
                 title = NSLocalizedString(@"ForceUpdateInfo", nil);
             }
             [AlertManager showAlertWithTitle:title message:nil cancelButtonTitle:NSLocalizedString(@"Update", nil) otherButtonTitles:otherButtons viewController:weakSelf completionHandler:^(NSInteger buttonClicked) {
                 [[Manager sharedInstance] removeBlurEffect];
                 if (buttonClicked == 0)
                 {
                     [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/tr/app/throwback/id1133004484"]];
                 }
                 else
                 {
                     [strongSelf animateLogo];
                 }
                 [[Manager sharedInstance] removeBlurEffect];
             }];
         }
     } failureBlock:^(NSError *error) {
         [AlertManager showTSAlertWithTitle:NSLocalizedString(@"Error", nil) message:error.localizedDescription type:TSMessageNotificationTypeError];
         [weakSelf animateLogo];
     }];
}

- (void)viewWillDisappear:(BOOL)animated
{
    self.finished = YES;
    self.asked = NO;
    [super viewWillDisappear:animated];
}

- (void)initUI
{
    self.tbButton.layer.cornerRadius = self.tbButton.frame.size.height / 2;
    self.tbButton.layer.masksToBounds = YES;
    self.tbButton.layer.borderWidth = 0.5f;
    self.infoLabel.text = NSLocalizedString(@"infoLogin", nil);
    self.letsTBLabel.text = NSLocalizedString(@"letsTB", nil);
}

- (void)animateLogo
{
    __weak __typeof(self)weakSelf = self;
    [UIView animateWithDuration:0.3f
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         weakSelf.circularProgressBarView.hidden = NO;
                         weakSelf.asked = NO;
                         [weakSelf.view layoutIfNeeded];
                     } completion:^(BOOL finished) {
                         [weakSelf.circularProgressBarView setValue:1000 animateWithDuration:0.6f];
                         [NSTimer scheduledTimerWithTimeInterval:0.5f target:self selector:@selector(rotateLogo) userInfo:nil repeats:NO];
                         [NSTimer scheduledTimerWithTimeInterval:0.8f target:self selector:@selector(checkUserLoginState) userInfo:nil repeats:NO];
                     }];
}

- (void)rotateLogo
{
    __weak __typeof(self)weakSelf = self;
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^{
        weakSelf.circularProgressBarView.transform = CGAffineTransformMakeRotation(M_PI);
        [weakSelf.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.4 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^{
            weakSelf.circularProgressBarView.transform = CGAffineTransformMakeRotation(0);
            [weakSelf.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            if (!strongSelf.finished) [strongSelf rotateLogo];
        }];
    }];
}

- (void)checkUserLoginState
{
    self.asked = YES;
    if ([UserManager loadUser] && [[Digits sharedInstance] session])
    {
        __weak __typeof(self)weakSelf = self;
        [[NetworkManager sharedInstance] authenticateWithUser:[UserManager loadUser] withSuccessBlock:^(NSString *token) {
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            if ([token isEqualToString:@"error"])
            {
                [AlertManager showAlertWithTitle:NSLocalizedString(@"Error", nil) message:nil cancelButtonTitle:NSLocalizedString(@"Done", nil) otherButtonTitles:@[NSLocalizedString(@"Retry", nil)] viewController:weakSelf completionHandler:^(NSInteger buttonClicked) {
                    if (buttonClicked == 1)
                    {
                        [weakSelf checkUserLoginState];
                    }
                }];
                [UIView animateWithDuration:0.3f
                                      delay:0.0f
                                    options:UIViewAnimationOptionCurveEaseIn
                                 animations:^{
                                     weakSelf.tbVeriticalConstraint.constant = -105;
                                     weakSelf.circularVerticalConstraint.constant = -105;
                                     [weakSelf.view layoutIfNeeded];
                                 } completion:^(BOOL finished) {
                                     [UIView animateWithDuration:0.3f
                                                           delay:0.0f
                                                         options:UIViewAnimationOptionCurveEaseIn
                                                      animations:^{
                                                          weakSelf.infoLabel.hidden = NO;
                                                          weakSelf.letsTBLabel.hidden = NO;
                                                          weakSelf.tbButton.hidden = NO;
                                                          [weakSelf.view layoutIfNeeded];
                                                      } completion:^(BOOL finished) {
                                                          weakSelf.finished = YES;
                                                      }];
                                 }];
            }
            else
            {
                [Manager sharedInstance].accessToken = token;
                strongSelf.finished = YES;
                [[Manager sharedInstance] registerForRemoteNotificationsWithSuccessBlock:^(BOOL success) {
                    UINavigationController *mainVC = (UINavigationController *)[[Manager sharedInstance] buildViewControllerFromStoryBoard:@"Main" withViewControllerId:@"MainNavController"];
                    [strongSelf.navigationController presentViewController:mainVC animated:YES completion:nil];
                }];
            }
        } failureBlock:^(NSError *error) {
            [AlertManager showErrorAlertWithError:error otherButtonTitles:@[NSLocalizedString(@"Retry", nil)] viewController:weakSelf completionHandler:^(NSInteger buttonClicked) {
                if (buttonClicked == 1)
                {
                    [weakSelf checkUserLoginState];
                }
            }];
            [UIView animateWithDuration:0.3f
                                  delay:0.0f
                                options:UIViewAnimationOptionCurveEaseIn
                             animations:^{
                                 weakSelf.tbVeriticalConstraint.constant = -105;
                                 weakSelf.circularVerticalConstraint.constant = -105;
                                 [weakSelf.view layoutIfNeeded];
                             } completion:^(BOOL finished) {
                                 [UIView animateWithDuration:0.3f
                                                       delay:0.0f
                                                     options:UIViewAnimationOptionCurveEaseIn
                                                  animations:^{
                                                      weakSelf.infoLabel.hidden = NO;
                                                      weakSelf.letsTBLabel.hidden = NO;
                                                      weakSelf.tbButton.hidden = NO;
                                                      [weakSelf.view layoutIfNeeded];
                                                  } completion:^(BOOL finished) {
                                                      weakSelf.finished = YES;
                                                  }];
                             }];
        }];
    }
    else
    {
        __weak __typeof(self)weakSelf = self;
        [UIView animateWithDuration:0.3f
                              delay:0.0f
                            options:UIViewAnimationOptionCurveEaseIn
                         animations:^{
                             weakSelf.tbVeriticalConstraint.constant = -105;
                             weakSelf.circularVerticalConstraint.constant = -105;
                             [weakSelf.view layoutIfNeeded];
                         } completion:^(BOOL finished) {
                             [UIView animateWithDuration:0.3f
                                                   delay:0.0f
                                                 options:UIViewAnimationOptionCurveEaseIn
                                              animations:^{
                                                  weakSelf.infoLabel.hidden = NO;
                                                  weakSelf.letsTBLabel.hidden = NO;
                                                  weakSelf.tbButton.hidden = NO;
                                                  [weakSelf.view layoutIfNeeded];
                                              } completion:^(BOOL finished) {
                                                  weakSelf.finished = YES;
                                              }];
                         }];
    }
}

- (IBAction)tbButtonTapped:(id)sender
{
    Digits *digits = [Digits sharedInstance];
    DGTAuthenticationConfiguration *configuration = [[DGTAuthenticationConfiguration alloc] initWithAccountFields:DGTAccountFieldsDefaultOptionMask];
    configuration.appearance = [[DGTAppearance alloc] init];
    configuration.appearance.backgroundColor = [UIColor whiteColor];
    configuration.appearance.accentColor = TB_BLUE;
    configuration.appearance.logoImage = [UIImage imageNamed:@"twit_logo"];
    [digits authenticateWithViewController:nil configuration:configuration completion:^(DGTSession *session, NSError *error) {
        // Inspect session/error objects
        if (!error)
        {
            TBCreateUserViewController *createUser = (TBCreateUserViewController *)[[Manager sharedInstance] buildViewControllerFromStoryBoard:@"Login" withViewControllerId:@"TBCreateUserViewController"];
            createUser.session = session;
            [self.navigationController pushViewController:createUser animated:YES];
        }
    }];
}

@end
