//
//  TBFriendCollectionViewCell.h
//  Throwback
//
//  Created by Onur Şahindur on 01/07/16.
//  Copyright © 2016 Throwback Corp. All rights reserved.
//

#import "MBCircularProgressBarView.h"

@interface TBFriendCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic)     IBOutlet    UILabel *contactNameLabel;
@property (weak, nonatomic)     IBOutlet    UILabel *phoneNumberLabel;
@property (weak, nonatomic)     IBOutlet    MBCircularProgressBarView *circularView;
@property (weak, nonatomic)     IBOutlet    UILabel *hashtagLabel;
@property (assign, nonatomic)   BOOL        userSelected;

@end
