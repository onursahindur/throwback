//
//  TBSettingsViewController.m
//  Throwback
//
//  Created by Onur Şahindur on 03/07/16.
//  Copyright © 2016 Throwback Corp. All rights reserved.
//

typedef enum
{
    TBRegionEnglish,
    TBRegionTurkish
}TBRegion;

#import "TBSettingsViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "TBThirdPartiesViewController.h"
@import MessageUI;

@interface TBSettingsViewController () <UITableViewDataSource, UITableViewDelegate, MFMailComposeViewControllerDelegate>

@property (assign, nonatomic) TBRegion currentRegion;

@property (weak, nonatomic) IBOutlet UILabel        *notificationNameLabel;
@property (weak, nonatomic) IBOutlet UITextField    *notificationNameTextField;
@property (weak, nonatomic) IBOutlet UIButton       *saveNotificationNameButton;
@property (weak, nonatomic) IBOutlet UILabel        *logoutLabel;
@property (weak, nonatomic) IBOutlet UILabel        *whoAreWeLabel;
@property (weak, nonatomic) IBOutlet UILabel        *feedBackLabel;
@property (weak, nonatomic) IBOutlet UILabel        *followUsLabel;
@property (weak, nonatomic) IBOutlet UILabel        *versionLabel;
@property (weak, nonatomic) IBOutlet UIButton       *facebookButton;
@property (weak, nonatomic) IBOutlet UIButton       *instagramButton;
@property (weak, nonatomic) IBOutlet UIButton       *twitterButton;
@property (weak, nonatomic) IBOutlet UIImageView    *notificationNameArrow;
@property (weak, nonatomic) IBOutlet UIImageView    *whoAreWeArrow;
@property (weak, nonatomic) IBOutlet UISwitch       *soundSwitch;
@property (weak, nonatomic) IBOutlet UILabel        *thirdPartyLabel;

@property (strong, nonatomic) NSMutableArray *expandedCells;

@end

@implementation TBSettingsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initUI];
    [AnalyticsManager trackScreenWithName:@"Settings"];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self localizeStrings];
}

- (void)initUI
{
    [NavigationManager addLeftBarButtonWithTitle:NSLocalizedString(@"Close", nil) image:nil target:self selector:@selector(closeSettings:) withFont:nil viewController:self];
    [NavigationManager styleNavigationTitleWithText:NSLocalizedString(@"Settings", nil) withViewController:self];
    self.navigationController.navigationBar.tintColor = TB_BLUE;
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:TB_BLUE}];
    self.navigationController.navigationBar.translucent = NO;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.facebookButton.layer.cornerRadius = self.facebookButton.frame.size.height / 2;
    self.facebookButton.layer.masksToBounds = YES;
    self.facebookButton.layer.borderWidth = 0.5f;
    self.instagramButton.layer.cornerRadius = self.instagramButton.frame.size.height / 2;
    self.instagramButton.layer.masksToBounds = YES;
    self.instagramButton.layer.borderWidth = 0.5f;
    self.twitterButton.layer.cornerRadius = self.twitterButton.frame.size.height / 2;
    self.twitterButton.layer.masksToBounds = YES;
    self.twitterButton.layer.borderWidth = 0.5f;
    
    self.expandedCells = [NSMutableArray new];
    
    self.notificationNameTextField.text = [UserManager loadUser].userName;
    
    self.saveNotificationNameButton.layer.cornerRadius = 10; // this value vary as per your desire
    self.saveNotificationNameButton.clipsToBounds = YES;
    
    NSString *appVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    self.versionLabel.text = [NSString stringWithFormat:@"v%@", appVersion];
}

- (void)localizeStrings
{
    self.notificationNameLabel.text = NSLocalizedString(@"NotificationName", nil);
    self.logoutLabel.text = NSLocalizedString(@"Logout", nil);
    self.whoAreWeLabel.text = NSLocalizedString(@"WhoAreWe", nil);
    self.feedBackLabel.text = NSLocalizedString(@"FeedBack", nil);
    self.followUsLabel.text = NSLocalizedString(@"FollowUs", nil);
    [self.saveNotificationNameButton setTitle:NSLocalizedString(@"Save", nil) forState:UIControlStateNormal];
    self.thirdPartyLabel.text = NSLocalizedString(@"ThirdParty", nil);
}

- (void)logoutButtonTapped:(id)sender
{
    __weak typeof(self)weakSelf = self;
    [[NetworkManager sharedInstance] logoutWithSuccessBlock:^(BOOL success) {
        [UserManager clearSavedUser];
        [Manager sharedInstance].accessToken = nil;
        __weak typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf dismissViewControllerAnimated:YES completion:^{
            if ([strongSelf.delegate respondsToSelector:@selector(settingsVCLogoutButtonTapped)])
            {
                [strongSelf.delegate settingsVCLogoutButtonTapped];
            }
        }];
    } failureBlock:^(NSError *error) {
        [AlertManager showTSAlertWithTitle:NSLocalizedString(@"Error", nil) message:error.localizedDescription type:TSMessageNotificationTypeError];
    }];
}

- (void)closeSettings:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UITableView Datasource & delegate
- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    UITableViewHeaderFooterView *v = (UITableViewHeaderFooterView *)view;
    v.backgroundView.backgroundColor = RGB(240, 240, 240);
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(section == 1)
    {
        return NSLocalizedString(@"account", nil);
    }
    else if(section == 2)
    {
        return NSLocalizedString(@"info", nil);;
    }
    return @"";
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.notificationNameTextField resignFirstResponder];
    // Account section
    if (indexPath.section == 1)
    {
        if (indexPath.row == 0)
        {
            [self expandCells:indexPath withUIImageView:self.notificationNameArrow];
        }
        else if (indexPath.row == 1)
        {
            [AlertManager showAlertWithTitle:NSLocalizedString(@"SureLogout", nil)  message:nil cancelButtonTitle:NSLocalizedString(@"Cancel", nil)  otherButtonTitles:@[NSLocalizedString(@"Logout", nil) ] viewController:self completionHandler:^(NSInteger buttonClicked) {
                if (buttonClicked == 1)
                {
                    [self logoutButtonTapped:nil];
                }
            }];
        }
    }
    // Info section
    else if (indexPath.section == 2)
    {
        if (indexPath.row == 0)
        {
            UIViewController *whoAreWeVC = [[Manager sharedInstance] buildViewControllerFromStoryBoard:@"Main" withViewControllerId:@"TBWhoAreWeViewController"];
            [self.navigationController pushViewController:whoAreWeVC animated:YES];
        }
        else if (indexPath.row == 1)
        {
            [self sendFeedBack];
        }
        else if (indexPath.row == 2)
        {
            
        }
        else if (indexPath.row == 3)
        {
            TBThirdPartiesViewController *whoAreWeVC = (TBThirdPartiesViewController *)[[Manager sharedInstance] buildViewControllerFromStoryBoard:@"Main" withViewControllerId:@"TBThirdPartiesViewController"];
            [self.navigationController pushViewController:whoAreWeVC animated:YES];
        }
    }
}

- (void)expandCells:(NSIndexPath *)indexPath
    withUIImageView:(UIImageView *)arrowView
{
    if ([self.expandedCells containsObject:indexPath])
    {
        [self.expandedCells removeObject:indexPath];
        arrowView.transform = CGAffineTransformMakeRotation(0);
    }
    else
    {
        if (indexPath.section == 1 && indexPath.row == 0) [self.notificationNameTextField becomeFirstResponder];
        [self.expandedCells addObject:indexPath];
        arrowView.transform = CGAffineTransformMakeRotation(M_PI_2);
    }
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.expandedCells containsObject:indexPath])
    {
        return 100.0f; //It's not necessary a constant, though
    }
    else
    {
        return 44.0f; //Again not necessary a constant
    }
}

#pragma mark - Actions
- (IBAction)saveNotifNameButtonTapped:(id)sender
{
    if ([self.notificationNameTextField.text isEqualToString:@""])
    {
        [AlertManager showAlertWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"NameNil", nil) cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil viewController:self completionHandler:nil];
        return;
    }
    
    TBUser *user = [UserManager loadUser];
    user.userName = self.notificationNameTextField.text;
    __weak __typeof(self)weakSelf = self;
    [[NetworkManager sharedInstance] updateUser:[UserManager loadUser] withSuccessBlock:^(BOOL success) {
        __strong __typeof(weakSelf)strongSelf = weakSelf;
        if (success)
        {
            TBUser *updatedUser = [UserManager loadUser];
            updatedUser.userName = strongSelf.notificationNameTextField.text;
            [UserManager saveUser:updatedUser];
            [AlertManager showTSAlertWithTitle:NSLocalizedString(@"Success", nil) message:NSLocalizedString(@"UserNameChanged", nil) type:TSMessageNotificationTypeSuccess];
            [strongSelf.notificationNameTextField resignFirstResponder];
            [strongSelf expandCells:[NSIndexPath indexPathForRow:0 inSection:1] withUIImageView:strongSelf.notificationNameArrow];
        }
    } failureBlock:^(NSError *error) {
        [AlertManager showTSAlertWithTitle:NSLocalizedString(@"Error", nil) message:error.localizedDescription type:TSMessageNotificationTypeError];
    }];
}


- (void)sendFeedBack
{
    if([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController* composeVC = [[MFMailComposeViewController alloc] init];
        composeVC.mailComposeDelegate = self;
        [composeVC setToRecipients:@[@"appthrowback@gmail.com"]];
        [composeVC setSubject:[NSString stringWithFormat:@"Feedback from %@", [UserManager loadUser].phoneNumber]];
        [composeVC setMessageBody:@"\n" isHTML:NO];
        [self presentViewController:composeVC animated:YES completion:nil];
    }
}

- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)socialMediaButtonTapped:(id)sender
{
    NSString *urlString = @"";
    if (sender == self.facebookButton)
    {
        urlString = @"fb://profile/589308514583880";
    }
    else if (sender == self.instagramButton)
    {
        urlString = @"instagram://user?username=appthrowback";
    }
    else if (sender == self.twitterButton)
    {
        urlString = @"twitter:///user?screen_name=appthrowback";
    }
    
    if (![[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]])
    {
        // opening the app didn't work - let's open Safari
        if (sender == self.facebookButton)
        {
            urlString = @"https://www.facebook.com/589308514583880";
        }
        else if (sender == self.instagramButton)
        {
            urlString = @"https://www.instagram.com/appthrowback";
        }
        else if (sender == self.twitterButton)
        {
            urlString = @"https://www.twitter.com/appthrowback";
        }
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
    }
}

- (IBAction)soundSwitchedChanged:(id)sender
{
    
}




@end
