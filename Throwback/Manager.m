//
//  Manager.m
//  Throwback
//
//  Created by Onur Şahindur on 01/07/16.
//  Copyright © 2016 Throwback Corp. All rights reserved.
//

#import "Manager.h"
#import "TBSorryViewController.h"

@interface Manager ()

@property (nonatomic, strong) UIVisualEffectView    *visualEffectView;

@end

@implementation Manager

+ (instancetype)sharedInstance
{
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [Manager new];
        [(Manager *)sharedInstance makeInitializationConfigurations];
    });
    return sharedInstance;
}

- (void)makeInitializationConfigurations
{
    self.visualEffectView = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleLight]];
}

- (UIViewController *)buildViewControllerFromStoryBoard:(NSString *)storyboardName
                                   withViewControllerId:(NSString *)vcID
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:vcID];
}

- (void)showMaintanenceAlert
{
    TBSorryViewController  *sorryVC = (TBSorryViewController *)[[Manager sharedInstance] buildViewControllerFromStoryBoard:@"Login" withViewControllerId:@"TBSorryViewController"];
    [[UIViewController currentViewController] presentViewController:sorryVC animated:YES completion:nil];
}

- (void)addBlurEffectToView:(UIView *)view
{
    self.visualEffectView.frame = [UIApplication sharedApplication].keyWindow.bounds;
    [view addSubview:self.visualEffectView];
    self.visualEffectView.layer.zPosition = 1;
}

- (void)removeBlurEffect
{
    self.visualEffectView.layer.zPosition = 0;
    [self.visualEffectView removeFromSuperview];
}

- (void)registerForRemoteNotificationsWithSuccessBlock:(void (^)(BOOL))successBlock
{
    AppDelegate* appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [appDelegate.oneSignal registerForPushNotifications];
    [appDelegate.oneSignal setSubscription:true];
    [appDelegate.oneSignal IdsAvailable:^(NSString *userId, NSString *pushToken) {
        [Manager sharedInstance].currentUserPushId      = userId;
        [Manager sharedInstance].currentUserPushToken   = pushToken;
        successBlock(YES);
    }];
}

- (UIViewController*)topMostController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    while (topController.presentedViewController)
    {
        topController = topController.presentedViewController;
    }
    return topController;
}

@end
