//
//  AppDelegate.m
//  Throwback
//
//  Created by Onur Şahindur on 01/07/16.
//  Copyright © 2016 Throwback Corp. All rights reserved.
//

#import "AppDelegate.h"
#import <AWSCore/AWSCore.h>
#import <AWSCognito/AWSCognito.h>
#import "TBDownloadViewController.h"
#import "TBMainViewController.h"
#import "TBLoginViewController.h"
#import <Fabric/Fabric.h>
#import <DigitsKit/DigitsKit.h>
#import <Fabric/Fabric.h>
#import <DigitsKit/DigitsKit.h>
#import <AudioToolbox/AudioToolbox.h>
#import <Crashlytics/Crashlytics.h>
#import <Google/Analytics.h>

static NSString *const kS3IdentityPoolID    = @"us-east-1:eeb6333d-02bf-4843-9bec-9f0544c9287a";
static NSString *const kOneSignalAppID      = @"56e87f34-3a83-4aac-a0e1-44b680fac66d";

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    // AWS Conf.
    AWSCognitoCredentialsProvider *credentialsProvider = [[AWSCognitoCredentialsProvider alloc] initWithRegionType:AWSRegionUSEast1 identityPoolId:kS3IdentityPoolID];
    AWSServiceConfiguration *configuration = [[AWSServiceConfiguration alloc] initWithRegion:AWSRegionUSEast1 credentialsProvider:credentialsProvider];
    AWSServiceManager.defaultServiceManager.defaultServiceConfiguration = configuration;
    
    // One-Signal
    self.oneSignal = [[OneSignal alloc] initWithLaunchOptions:launchOptions
                                                        appId:kOneSignalAppID
                                           handleNotification:^(NSString* message, NSDictionary* additionalData, BOOL isActive) {
                                               NSLog(@"OneSignal Notification opened:\nMessage: %@", message);
                                               if (additionalData)
                                               {
                                                   DebugLog(@"additionalData: %@", additionalData);
                                                   // Check for and read any custom values you added to the notification
                                                   // This done with the "Additonal Data" section the dashbaord.
                                                   // OR setting the 'data' field on our REST API.
                                                   NSString* customKey = additionalData[@"customKey"];
                                                   if (customKey)
                                                       NSLog(@"customKey: %@", customKey);
                                               }
                                           } autoRegister:NO];
    
    // Digits and Crashlytics
    [Fabric with:@[[AWSCognito class], [Digits class], [Crashlytics class]]];
    
    // Google Analytics
    NSError *configureError;
    [[GGLContext sharedInstance] configureWithError:&configureError];
    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
    GAI *gai = [GAI sharedInstance];
    gai.trackUncaughtExceptions = YES;  // report uncaught exceptions
    
    return YES;
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    if ([userInfo objectForKey:@"custom"])
    {
        if ([[userInfo objectForKey:@"custom"] objectForKey:@"a"])
        {
            if ([[[userInfo objectForKey:@"custom"] objectForKey:@"a"] objectForKey:@"activity"])
            {
                UIViewController *currentVC = [UIViewController currentViewController];
                if (application.applicationState == UIApplicationStateActive)
                {
                    AudioServicesPlaySystemSound(1309);
                    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
                    
                    [TSMessage addCustomDesignFromFileWithName:@"ThrowbackMessageDesign.json"];
                    [TSMessage showNotificationInViewController:currentVC
                                                          title:nil
                                                       subtitle:userInfo[@"aps"][@"alert"]
                                                          image:nil
                                                           type:TSMessageNotificationTypeMessage
                                                       duration:TSMessageNotificationDurationAutomatic
                                                       callback:nil
                                                    buttonTitle:NSLocalizedString(@"Download", nil)
                                                 buttonCallback:^{
                                                     [self openViewControllerFromNotificationWithViewController:currentVC
                                                                                                   withUserInfo:userInfo
                                                                                           withApplicationState:application.applicationState];
                                                 }
                                                     atPosition:TSMessageNotificationPositionNavBarOverlay
                                           canBeDismissedByUser:YES];
                }
                else
                {
                    [self openViewControllerFromNotificationWithViewController:currentVC
                                                                  withUserInfo:userInfo
                                                          withApplicationState:application.applicationState];
                }
            }
            else if ([[[userInfo objectForKey:@"custom"] objectForKey:@"a"] objectForKey:@"name"])
            {
                if (application.applicationState == UIApplicationStateActive)
                {
                    AudioServicesPlaySystemSound(1309);
                    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
                }
                [AlertManager showTSAlertWithTitle:[[userInfo objectForKey:@"aps"] objectForKey:@"alert"] message:nil type:TSMessageNotificationTypeSuccess];
            }
        }
    }
}

- (void)openViewControllerFromNotificationWithViewController:(UIViewController *)currentVC
                                                withUserInfo:(NSDictionary *)userInfo
                                        withApplicationState:(UIApplicationState)applicationState
{
    if ([currentVC isKindOfClass:[TBLoginViewController class]])
    {
        [Manager sharedInstance].activityLogFromNotification = [[TBActivityLog alloc] initWithDictionary:[[[userInfo objectForKey:@"custom"] objectForKey:@"a"] objectForKey:@"activity"]];
    }
    else
    {
        NSString *activtyID = [[[[userInfo objectForKey:@"custom"] objectForKey:@"a"] objectForKey:@"activity"] objectForKey:@"_id"];
        [[NetworkManager sharedInstance] checkActivityStatus:activtyID withSuccessBlock:^(NSString *activityStatus) {
            if (![activityStatus isEqualToString:@"downloaded"])
            {
                if ([currentVC isKindOfClass:[TBMainViewController class]])
                {
                    TBDownloadViewController *dlListVC = (TBDownloadViewController *)[[Manager sharedInstance] buildViewControllerFromStoryBoard:@"Main" withViewControllerId:@"TBDownloadViewController"];
                    dlListVC.activityFromNotification = [[TBActivityLog alloc] initWithDictionary:[[[userInfo objectForKey:@"custom"] objectForKey:@"a"] objectForKey:@"activity"]];
                    [[UIViewController currentViewController].navigationController pushViewController:dlListVC animated:YES];
                }
                else if ([currentVC isKindOfClass:[TBDownloadViewController class]])
                {
                    // Call its download method.
                    if (applicationState != UIApplicationStateActive)
                    {
                        ((TBDownloadViewController *)currentVC).activityFromNotification = [[TBActivityLog alloc] initWithDictionary:[[[userInfo objectForKey:@"custom"] objectForKey:@"a"] objectForKey:@"activity"]];
                        [((TBDownloadViewController *)currentVC) askDownloadRequest];
                    }
                }
                else
                {
                    [currentVC dismissViewControllerAnimated:YES completion:^{
                        TBDownloadViewController *dlListVC = (TBDownloadViewController *)[[Manager sharedInstance] buildViewControllerFromStoryBoard:@"Main" withViewControllerId:@"TBDownloadViewController"];
                        dlListVC.activityFromNotification = [[TBActivityLog alloc] initWithDictionary:[[[userInfo objectForKey:@"custom"] objectForKey:@"a"] objectForKey:@"activity"]];
                        [[UIViewController currentViewController].navigationController pushViewController:dlListVC animated:YES];
                    }];
                }
            }
        } failureBlock:^(NSError *error) {
            [AlertManager showTSAlertWithTitle:NSLocalizedString(@"Error", nil) message:error.localizedDescription type:TSMessageNotificationTypeError];
        }];
    }
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
