//
//  TBContactListViewController.h
//  Throwback
//
//  Created by Onur Şahindur on 01/07/16.
//  Copyright © 2016 Throwback Corp. All rights reserved.
//

#import "TBViewController.h"

@class TBContactListViewController;
@protocol TBContactListViewControllerDelegate <NSObject>

- (void)contactListViewControllerDidSelectItem:(TBContactListViewController *)viewController
                            withRecieverUsers:(NSArray *)receivers;

@end

@interface TBContactListViewController : TBViewController

@property (nonatomic, strong) NSArray *selectedItems;

@property (nonatomic, weak) id <TBContactListViewControllerDelegate> delegate;

@end
