//
//  TBUser.h
//  Throwback
//
//  Created by Onur Şahindur on 03/07/16.
//  Copyright © 2016 Throwback Corp. All rights reserved.
//

@interface TBUser : NSObject

@property (nonatomic, strong) NSString *userID;
@property (nonatomic, strong) NSString *phoneNumber;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *notificationID;
@property (nonatomic, strong) NSString *regionType;
@property (nonatomic, strong) NSString *countryCode;
@property (nonatomic, strong) NSArray  *downloadList;
@property (nonatomic, strong) NSString *active;
@property (nonatomic, assign) BOOL     downloaded;

- (id)initWithDictionary:(NSDictionary *)user;

@end
