//
//  TBSentCollectionViewCell.m
//  Throwback
//
//  Created by Onur Şahindur on 01/07/16.
//  Copyright © 2016 Throwback Corp. All rights reserved.
//

#import "TBSentCollectionViewCell.h"

static CGFloat const kDeleteButtonWidth = 70.0f;

@interface TBSentCollectionViewCell ()

@property (weak, nonatomic) IBOutlet UIView *contentsView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewLeftConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewRightConstraint;

@property (assign, nonatomic) BOOL swiped;

@end

@implementation TBSentCollectionViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.backgroundColor = TB_BACKGROUND;
    //[self addSwipeGestures];
}

- (void)addSwipeGestures
{
    UISwipeGestureRecognizer *rightSw = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeLeft:)];
    rightSw.direction = UISwipeGestureRecognizerDirectionLeft;
    [self addGestureRecognizer:rightSw];
    UISwipeGestureRecognizer *leftSw = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeRight:)];
    leftSw.direction = UISwipeGestureRecognizerDirectionRight;
    [self addGestureRecognizer:leftSw];
}

//- (void)handleSwipeLeft:(id)sender
//{
//    [UIView animateWithDuration:0.08f
//                          delay:0.0f
//                        options:UIViewAnimationOptionCurveEaseIn
//                     animations:^{
//                         self.contentViewLeftConstraint.constant = -kDeleteButtonWidth;
//                         self.contentViewRightConstraint.constant = kDeleteButtonWidth;
//                         [self layoutIfNeeded];
//                     } completion:^(BOOL finished) {
//                         
//                     }];
//    
//}
//
//- (void)handleSwipeRight:(id)sender
//{
//    [UIView animateWithDuration:0.08f
//                          delay:0.0f
//                        options:UIViewAnimationOptionCurveEaseIn
//                     animations:^{
//                         self.contentViewLeftConstraint.constant = 0.0f;
//                         self.contentViewRightConstraint.constant = 0.0f;
//                         [self layoutIfNeeded];
//                     } completion:^(BOOL finished) {
//                         
//                     }];
//}

- (IBAction)deleteButtonTapped:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(sentCollectionViewCellDeleteButtonTapped:withActivity:)])
    {
        //[self handleSwipeRight:nil];
        [self.delegate sentCollectionViewCellDeleteButtonTapped:self withActivity:self.activity];
    }
}


@end
