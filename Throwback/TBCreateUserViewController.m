//
//  TBCreateUserViewController.m
//  Throwback
//
//  Created by Onur Şahindur on 03/07/16.
//  Copyright © 2016 Throwback Corp. All rights reserved.
//

#import "TBCreateUserViewController.h"
#import "TBMainViewController.h"

@interface TBCreateUserViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel            *lastStepLabel;
@property (weak, nonatomic) IBOutlet UITextField        *userNameTextArea;
@property (weak, nonatomic) IBOutlet UIButton           *doneButton;

@end

@implementation TBCreateUserViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    UITapGestureRecognizer *dismissKB = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard:)];
    [self.view addGestureRecognizer:dismissKB];
    [NavigationManager styleNavigationTitleWithText:@"#throwback" withViewController:self];
    [self localize];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
    [self.userNameTextArea becomeFirstResponder];
}

- (void)localize
{
    self.userNameTextArea.placeholder = NSLocalizedString(@"userNameInfo", nil);
    self.lastStepLabel.text = NSLocalizedString(@"lastStep", nil);
    [self.doneButton setTitle:NSLocalizedString(@"Done", nil) forState:UIControlStateNormal];
}

- (void)dismissKeyboard:(id)sender
{
    [self.userNameTextArea resignFirstResponder];
}

- (IBAction)doneButtonTapped:(id)sender
{
    if ([self.userNameTextArea.text isEqualToString:@""])
    {
        [AlertManager showAlertWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"FillAll", nil) cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil viewController:self completionHandler:nil];
        return;
    }
    
    TBUser *newUser = [TBUser new];
    newUser.phoneNumber = self.session.phoneNumber;
    newUser.countryCode = [self.session.phoneNumber substringWithRange:NSMakeRange(0, self.session.phoneNumber.length - 10)];
    newUser.userName = self.userNameTextArea.text;
    newUser.userID = @"";
    newUser.regionType = [[NSLocale preferredLanguages] firstObject];
    
    // Register for remote notifications
    __weak typeof(self) weakSelf = self;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[Manager sharedInstance] registerForRemoteNotificationsWithSuccessBlock:^(BOOL success) {
        newUser.notificationID = [Manager sharedInstance].currentUserPushId;
        [[NetworkManager sharedInstance] registerUserWithUser:newUser withSuccessBlock:^(TBUser *returnedUser) {
            
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            [MBProgressHUD hideHUDForView:strongSelf.view animated:YES];
            [UserManager saveUser:returnedUser];
            
            [[NetworkManager sharedInstance] authenticateWithUser:[UserManager loadUser] withSuccessBlock:^(NSString *token)
            {
                __strong __typeof(weakSelf)strongSelf = weakSelf;
                if ([token isEqualToString:@"error"])
                {
                    [AlertManager showAlertWithTitle:NSLocalizedString(@"Error", nil) message:nil cancelButtonTitle:NSLocalizedString(@"Done", nil) otherButtonTitles:@[NSLocalizedString(@"Retry", nil)] viewController:strongSelf completionHandler:^(NSInteger buttonClicked) {
                        if (buttonClicked == 1)
                        {
                            [strongSelf doneButtonTapped:self.doneButton];
                        }
                    }];
                }
                else
                {
                    [Manager sharedInstance].accessToken = token;
                    UINavigationController *mainVC = (UINavigationController *)[[Manager sharedInstance] buildViewControllerFromStoryBoard:@"Main" withViewControllerId:@"MainNavController"];
                    [strongSelf.navigationController presentViewController:mainVC animated:YES completion:^{
                        [strongSelf.navigationController popToRootViewControllerAnimated:NO];
                    }];
                }
            } failureBlock:^(NSError *error) {
                [AlertManager showErrorAlertWithError:error otherButtonTitles:@[NSLocalizedString(@"Retry", nil)] viewController:weakSelf completionHandler:^(NSInteger buttonClicked) {
                    if (buttonClicked == 1)
                    {
                        [weakSelf doneButtonTapped:self.doneButton];
                    }
                }];
            }];
        } failureBlock:^(NSError *error) {
            [MBProgressHUD hideHUDForView:weakSelf.view animated:YES];
            [AlertManager showTSAlertWithTitle:NSLocalizedString(@"Error", nil) message:error.localizedDescription type:TSMessageNotificationTypeError];
        }];
    }];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (range.length + range.location > textField.text.length)
    {
        return NO;
    }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return newLength <= 15;
}

@end
