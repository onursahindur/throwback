//
//  NavigationManager.h
//  Throwback
//
//  Created by Onur Şahindur on 01/07/16.
//  Copyright © 2016 Throwback Corp. All rights reserved.
//

@interface NavigationManager : NSObject

+ (void)addLeftBarButtonWithTitle:(NSString *)title
                            image:(UIImage *)image
                           target:(id)target
                         selector:(SEL)selector
                         withFont:(UIFont *)font
                   viewController:(UIViewController *)viewController;

+ (void)addRightBarButtonWithTitle:(NSString *)title
                             image:(UIImage *)image
                            target:(id)target
                          selector:(SEL)selector
                          withFont:(UIFont *)font
                    viewController:(UIViewController *)viewController;

+ (void)styleNavigationTitleWithText:(NSString *)text
                  withViewController:(UIViewController *)viewController;

+ (void)styleNavigationTitleWithHeaderImageWithViewController:(UIViewController *)viewController;

@end
