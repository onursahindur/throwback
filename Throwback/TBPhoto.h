//
//  TBPhoto.h
//  Throwback
//
//  Created by Onur Şahindur on 23/07/16.
//  Copyright © 2016 Throwback Corp. All rights reserved.
//

#import "NYTPhoto.h"

@interface TBPhoto : NSObject <NYTPhoto>

@property (nonatomic, strong) UIImage               *image;
@property (nonatomic, strong) NSData                *imageData;
@property (nonatomic, strong) UIImage               *placeholderImage;
@property (nonatomic, strong) NSAttributedString    *attributedCaptionTitle;
@property (nonatomic, strong) NSAttributedString    *attributedCaptionSummary;
@property (nonatomic, strong) NSAttributedString    *attributedCaptionCredit;

@end
