//
//  TBSorryViewController.m
//  Throwback
//
//  Created by Onur Şahindur on 16/07/16.
//  Copyright © 2016 Throwback Corp. All rights reserved.
//

#import "TBSorryViewController.h"

@interface TBSorryViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *maint1;
@property (weak, nonatomic) IBOutlet UIImageView *maint2;
@property (weak, nonatomic) IBOutlet UIImageView *maint3;
@property (weak, nonatomic) IBOutlet UILabel *serverDownLabel;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;

@end

@implementation TBSorryViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self startAnimating];
    [self.navigationController setNavigationBarHidden:NO];
    self.serverDownLabel.text = NSLocalizedString(@"serverDown", nil);
    [self.doneButton setTitle:NSLocalizedString(@"Done", nil) forState:UIControlStateNormal];
    self.maint1.image = [self.maint1.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.maint2.image = [self.maint2.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.maint3.image = [self.maint3.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
}

- (void)startAnimating
{
    __weak typeof(self)weakSelf = self;
    [UIView animateWithDuration:1.0f delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^{
        weakSelf.maint1.transform = CGAffineTransformMakeRotation(M_PI);
        weakSelf.maint2.transform = CGAffineTransformMakeRotation(M_PI);
        weakSelf.maint3.transform = CGAffineTransformMakeRotation(M_PI);
        [weakSelf.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:1.0f delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^{
            weakSelf.maint1.transform = CGAffineTransformMakeRotation(0);
            weakSelf.maint2.transform = CGAffineTransformMakeRotation(0);
            weakSelf.maint3.transform = CGAffineTransformMakeRotation(0);
            [weakSelf.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            [weakSelf startAnimating];
        }];
    }];
}

- (IBAction)doneButtonTapped:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

@end
