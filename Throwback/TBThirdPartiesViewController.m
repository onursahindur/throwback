//
//  TBThirdPartiesViewController.m
//  Throwback
//
//  Created by Onur Şahindur on 18/07/16.
//  Copyright © 2016 Throwback Corp. All rights reserved.
//

#import "TBThirdPartiesViewController.h"
#import "TBThirdPartyTableViewCell.h"

@interface TBThirdPartiesViewController () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet    UITableView    *tableView;
@property (strong, nonatomic)           NSDictionary   *thirdPartiesDict;
@property (strong, nonatomic)           NSMutableArray *expandedCells;

@end

@implementation TBThirdPartiesViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.tableView registerNib:[UINib nibWithNibName:@"TBThirdPartyTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"TBThirdPartyTableViewCell"];
    self.expandedCells = [NSMutableArray new];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.thirdPartiesDict = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"ThirdParties" ofType:@"plist"]];
    [self.tableView reloadData];
}

#pragma mark - TableView methods
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.expandedCells containsObject:indexPath])
    {
        return 345.0f;
    }
    else
    {
        return 44.0f;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.thirdPartiesDict allKeys].count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TBThirdPartyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TBThirdPartyTableViewCell" forIndexPath:indexPath];
    cell.thirdPartyNameLabel.text = [self.thirdPartiesDict.allKeys objectAtIndex:indexPath.row];
    cell.thirdPartyValueLabel.text = [self.thirdPartiesDict.allValues objectAtIndex:indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell.thirdPartyValueLabel setContentOffset:CGPointZero];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TBThirdPartyTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [self expandCells:indexPath withUIImageView:cell.arrowImageView];
    [cell.thirdPartyValueLabel setContentOffset:CGPointZero];
}

- (void)expandCells:(NSIndexPath *)indexPath
    withUIImageView:(UIImageView *)arrowView
{
    if ([self.expandedCells containsObject:indexPath])
    {
        [self.expandedCells removeObject:indexPath];
        arrowView.transform = CGAffineTransformMakeRotation(0);
    }
    else
    {
        [self.expandedCells addObject:indexPath];
        arrowView.transform = CGAffineTransformMakeRotation(M_PI_2);
    }
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

@end
