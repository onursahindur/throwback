//
//  NavigationManager.m
//  Throwback
//
//  Created by Onur Şahindur on 01/07/16.
//  Copyright © 2016 Throwback Corp. All rights reserved.
//

#import "NavigationManager.h"

@implementation NavigationManager

+ (void)addLeftBarButtonWithTitle:(NSString *)title
                            image:(UIImage *)image
                           target:(id)target
                         selector:(SEL)selector
                         withFont:(UIFont *)font
                   viewController:(UIViewController *)viewController
{
    UIBarButtonItem *button;
    if (image)
    {
        button = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStylePlain target:target action:selector];
    }
    else
    {
        button = [[UIBarButtonItem alloc] initWithTitle:title style:UIBarButtonItemStylePlain target:target action:selector];
    }
    if (font) [button setTitleTextAttributes:@{NSFontAttributeName:font} forState:UIControlStateNormal];
    viewController.navigationItem.leftBarButtonItem = button;
}

+ (void)addRightBarButtonWithTitle:(NSString *)title
                             image:(UIImage *)image
                            target:(id)target
                          selector:(SEL)selector
                          withFont:(UIFont *)font
                    viewController:(UIViewController *)viewController
{
    UIBarButtonItem *button;
    if (image)
    {
        button = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStylePlain target:target action:selector];
    }
    else
    {
        button = [[UIBarButtonItem alloc] initWithTitle:title style:UIBarButtonItemStylePlain target:target action:selector];
    }
    if (font) [button setTitleTextAttributes:@{NSFontAttributeName:font} forState:UIControlStateNormal];
    viewController.navigationItem.rightBarButtonItem = button;
}

+ (void)styleNavigationTitleWithText:(NSString *)text
                  withViewController:(UIViewController *)viewController
{
    viewController.navigationItem.titleView = nil;
    viewController.navigationItem.title = text;
}

+ (void)styleNavigationTitleWithHeaderImageWithViewController:(UIViewController *)viewController
{
    viewController.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"headerLogo"]];
    viewController.navigationController.navigationBar.translucent = NO;
}

@end
