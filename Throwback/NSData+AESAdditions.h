//
//  NSData+AESAdditions.h
//  StarTv_Carbon
//
//  Created by Onur Şahindur on 23/07/16.
//  Copyright © 2016 mobilike. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (AESAdditions)

- (NSData *)AES256EncryptWithKey:(NSString*)key;
- (NSData *)AES256DecryptWithKey:(NSString*)key;

@end
