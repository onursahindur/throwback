//
//  TBMainViewController.m
//  Throwback
//
//  Created by Onur Şahindur on 01/07/16.
//  Copyright © 2016 Throwback Corp. All rights reserved.
//

#import "TBMainViewController.h"
#import "TBSentCollectionViewCell.h"
#import "QBImagePickerController.h"
#import "TBContactListViewController.h"
#import "TBSettingsViewController.h"
#import <AWSCore/AWSCore.h>
#import <AWSS3/AWSS3.h>
#import "TBActivityLog.h"
#import "TBDownloadViewController.h"
#import "TSMessage.h"
#import "UIViewController+Utils.h"
#import "MBCircularProgressBarView.h"
#import "TBTutorialViewController.h"
#import "TBUsersDownloadedViewController.h"
#import "UIBarButtonItem+Badge.h"

static CGFloat      const kCellHeight                       = 70.0f;
static NSInteger    const kMaximumSelection                 = 50;

static CGFloat      const kInProgressActvityLogConst        = 200.0f;
static CGFloat      const kActvityLogConst                  = 8.0f;
static CGFloat      const kCollectionViewOffset             = 20.0f;

static NSUInteger   const kAllowedByteSize                  = 600000;

@interface TBMainViewController () <UICollectionViewDelegate, UICollectionViewDataSource, QBImagePickerControllerDelegate, TBTutorialViewControllerDelegate,
                                                TBSettingsViewControllerDelegate, TBSentCollectionViewCellDelegate, TBContactListViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIButton                           *tbButton;
@property (weak, nonatomic) IBOutlet UILabel                            *activityLogLabel;
@property (weak, nonatomic) IBOutlet UIView                             *inProgressView;
@property (weak, nonatomic) IBOutlet UILabel                            *uploadingLabel;
@property (weak, nonatomic) IBOutlet UILabel                            *recieverNameLabel;
@property (weak, nonatomic) IBOutlet MBCircularProgressBarView          *circularUploadBarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint                 *tbButtonHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint                 *tbButtonWidthConstraint;


@property (strong, nonatomic) NSMutableArray                            *recentActivityArray;
@property (strong, nonatomic) NSMutableArray                            *uploadCollection;
@property (assign, nonatomic) NSInteger                                 collectionUploadCount;
@property (assign, nonatomic) NSInteger                                 collectionUploadErrorCount;
@property (strong, nonatomic) NSString                                  *uniqueIDForTransfer;
@property (strong, nonatomic) TBActivityLog                             *currentActivity;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint                 *activityLogTopConstraint;

@end

@implementation TBMainViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initUI];
    [AnalyticsManager trackScreenWithName:@"Main Screen"];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [NavigationManager styleNavigationTitleWithText:@"#throwback" withViewController:self];
    if ([Manager sharedInstance].activityLogFromNotification)
    {
        TBActivityLog *activityLog = [Manager sharedInstance].activityLogFromNotification;
        [Manager sharedInstance].activityLogFromNotification = nil;
        [self downloadImagesFromNotification:activityLog];
    }
    [self walkThrough];
}

- (void)initUI
{
    [self.collectionView registerNib:[UINib nibWithNibName:@"TBSentCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"TBSentCollectionViewCell"];
    self.navigationController.navigationBar.translucent = NO;
    self.tbButton.layer.cornerRadius = self.tbButton.frame.size.height / 2;
    self.tbButton.layer.masksToBounds = YES;
    self.tbButton.layer.borderWidth = 0.5f;
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(fetchActivityLog:)
             forControlEvents:UIControlEventValueChanged];
    [self.collectionView addSubview:refreshControl];
    self.collectionView.alwaysBounceVertical = YES;
    self.collectionView.contentInset = UIEdgeInsetsMake(0, 0, CGRectGetHeight(self.tbButton.frame) + kCollectionViewOffset, 0);
    self.activityLogLabel.text = NSLocalizedString(@"activitylog", nil);
    self.navigationItem.rightBarButtonItem.shouldHideBadgeAtZero = YES;
}

- (void)walkThrough
{
    BOOL walkThroughShowed = [[NSUserDefaults standardUserDefaults] boolForKey:@"walk"];
    if (!walkThroughShowed)
    {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"walk"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        TBTutorialViewController *tutorialViewController = (TBTutorialViewController *)[[Manager sharedInstance] buildViewControllerFromStoryBoard:@"Tutorial" withViewControllerId:@"TBTutorialViewController"];
        tutorialViewController.delegate = self;
        tutorialViewController.providesPresentationContextTransitionStyle = YES;
        tutorialViewController.definesPresentationContext = YES;
        [tutorialViewController setModalPresentationStyle:UIModalPresentationOverCurrentContext];
        [self.navigationController presentViewController:tutorialViewController animated:YES completion:nil];
    }
    else
    {
        [self fetchActivityLog:nil];
    }
}

#pragma mark - TBTutorialViewControllerDelegate
- (void)tutorialViewControllerDidFinishTutorial
{
    [self fetchActivityLog:nil];
}

#pragma mark -
- (void)fetchActivityLog:(id)sender
{
    [self fetchDownloadListCount];
    if (!sender) [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    __weak typeof(self) weakSelf = self;
    [[NetworkManager sharedInstance] getRecentActivitiesWithUser:[UserManager loadUser] withSuccessBlock:^(NSArray *activities) {
        __strong __typeof(weakSelf)strongSelf = weakSelf;
        if (!sender)
        {
            [MBProgressHUD hideHUDForView:strongSelf.view animated:YES];
        }
        else
        {
            [(UIRefreshControl *)sender endRefreshing];
        }
        strongSelf.recentActivityArray = activities.mutableCopy;
        if (strongSelf.recentActivityArray.count == 0)
        {
            strongSelf.activityLogLabel.text = NSLocalizedString(@"noActivityLog", nil);
        }
        dispatch_async(dispatch_get_main_queue(), ^ {
            [strongSelf.collectionView reloadData];
        });
        
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            switch (status) {
                case PHAuthorizationStatusAuthorized:
                {
                    [[Manager sharedInstance] removeBlurEffect];
                }
                    break;
                case PHAuthorizationStatusRestricted:
                    break;
                case PHAuthorizationStatusDenied:
                {
                    [[Manager sharedInstance] addBlurEffectToView:strongSelf.view];
                    [AlertManager showAlertWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"PhotosWarning", nil) cancelButtonTitle:NSLocalizedString(@"Cancel", nil) otherButtonTitles:@[NSLocalizedString(@"Settings", nil)] viewController:self completionHandler:^(NSInteger buttonClicked)
                     {
                         if (buttonClicked == 0)
                         {
                             [[Manager sharedInstance] removeBlurEffect];
                         }
                         else
                         {
                             [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                         }
                     }];
                }
                    break;
                default:
                    break;
            }
        }];
        
    } failureBlock:^(NSError *error) {
        if (!sender)
        {
            [MBProgressHUD hideHUDForView:weakSelf.view animated:YES];
        }
        else
        {
            [(UIRefreshControl *)sender endRefreshing];
        }
        [AlertManager showTSAlertWithTitle:NSLocalizedString(@"Error", nil) message:error.localizedDescription type:TSMessageNotificationTypeError];
    }];
}

- (void)fetchDownloadListCount
{
    __weak typeof(self)weakSelf = self;
    [[NetworkManager sharedInstance] getDownloadListWithUser:[UserManager loadUser] withSuccessBlock:^(NSArray *downloadList) {
        weakSelf.navigationItem.rightBarButtonItem.badgeValue = [NSString stringWithFormat:@"%ld", downloadList.count];
    } failureBlock:^(NSError *error) {
        weakSelf.navigationItem.rightBarButtonItem.badgeValue = @"0";
    }];
}

#pragma mark - Collection View
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.recentActivityArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    TBSentCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TBSentCollectionViewCell" forIndexPath:indexPath];
    TBActivityLog *log = [self.recentActivityArray objectAtIndex:indexPath.row];
    cell.delegate = self;
    cell.activity = log;
    [cell.contentView setBackgroundColor:[UIColor clearColor]];
    if ([self userDownloadedActivity:log.recievers] && [self arrayContainsUser:log.recievers]) // Received
    {
        cell.sentUserNameLabel.text = [NSString stringWithFormat:NSLocalizedString(@"fromName", nil), log.ownerName];
        [cell.sentUserNameLabel setFont:[UIFont systemFontOfSize:15.0f]];
        [cell.statusImageView setImage:[UIImage imageNamed:@"recieved"]];
        cell.statusLabel.textColor = RGB(0, 90, 90);
        cell.statusLabel.text = NSLocalizedString(@"downloaded", nil);
    }
    else // Sent
    {
        if (log.recievers.count == 1)
        {
            cell.sentUserNameLabel.text = [NSString stringWithFormat:NSLocalizedString(@"sentName", nil), ((TBUser *)[log.recievers firstObject]).userName];
            [cell.sentUserNameLabel setFont:[UIFont systemFontOfSize:15.0f]];
            if (log.logType == TBActivityLogTypeUploaded)
            {
                cell.statusLabel.textColor = TB_ORANGE;
                cell.statusLabel.text = NSLocalizedString(@"uploaded", nil);
            }
            else if (log.logType == TBActivityLogTypeDownloaded)
            {
                cell.statusLabel.textColor = RGB(0, 90, 90);
                cell.statusLabel.text = NSLocalizedString(@"downloaded", nil);
            }
            else if (log.logType == TBActivityLogTypeCancelled)
            {
                cell.statusLabel.textColor = [UIColor redColor];
                cell.statusLabel.text = NSLocalizedString(@"cancelled", nil);
            }
        }
        else
        {
            cell.sentUserNameLabel.text = [NSString stringWithFormat:NSLocalizedString(@"sentNameMultiple", nil), log.recievers.count];
            [cell.sentUserNameLabel setFont:[UIFont italicSystemFontOfSize:15.0f]];
            if (log.logType == TBActivityLogTypeUploaded)
            {
                cell.statusLabel.textColor = TB_ORANGE;
                cell.statusLabel.text = NSLocalizedString(@"uploaded", nil);
            }
            else if (log.logType == TBActivityLogTypeDownloaded)
            {
                cell.statusLabel.textColor = RGB(0, 90, 90);
                cell.statusLabel.text = NSLocalizedString(@"downloaded", nil);
            }
            else if (log.logType == TBActivityLogTypeCancelled)
            {
                cell.statusLabel.textColor = [UIColor redColor];
                cell.statusLabel.text = NSLocalizedString(@"cancelled", nil);
            }
        }
        [cell.statusImageView setImage:[UIImage imageNamed:@"sent"]];
    }
    cell.photoCountLabel.text = [NSString stringWithFormat:NSLocalizedString(@"photoCount", nil), (long)log.numberOfPhotos];
    cell.timeLabel.text = log.shortWhenAnnotation;
    cell.statusImageView.image = [cell.statusImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    TBActivityLog *activity = (TBActivityLog *)[self.recentActivityArray objectAtIndex:indexPath.row];
    if (activity.recievers.count > 1 && !([self userDownloadedActivity:activity.recievers] && [self arrayContainsUser:activity.recievers]))
    {
        TBUsersDownloadedViewController *downloadedUsers = (TBUsersDownloadedViewController *)[[Manager sharedInstance] buildViewControllerFromStoryBoard:@"Main" withViewControllerId:@"TBUsersDownloadedViewController"];
        downloadedUsers.recieverArray = activity.recievers;
        downloadedUsers.providesPresentationContextTransitionStyle = YES;
        downloadedUsers.definesPresentationContext = YES;
        [downloadedUsers setModalPresentationStyle:UIModalPresentationOverCurrentContext];
        [self.navigationController presentViewController:downloadedUsers animated:YES completion:nil];
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(CGRectGetWidth(self.view.frame), kCellHeight);
}

#pragma mark - TBSentCollectionViewDelegate
- (void)sentCollectionViewCellDeleteButtonTapped:(TBSentCollectionViewCell *)cell
                                    withActivity:(TBActivityLog *)activity
{
    __weak typeof(self) weakSelf = self;
    [[NetworkManager sharedInstance] deleteSelectedActivityWithActivity:activity withSuccessBlock:^(BOOL success) {
        // Nothing to do here...
        __strong typeof(weakSelf) strongSelf = weakSelf;
        [strongSelf fetchActivityLog:nil];
    } failureBlock:^(NSError *error) {
        DebugLog(@"%@", error.localizedDescription);
    }];
}

#pragma mark - QBImagePickerController
- (void)qb_imagePickerController:(QBImagePickerController *)imagePickerController didFinishPickingAssets:(NSArray *)assets
{
    [self dismissViewControllerAnimated:YES completion:nil];
    TBContactListViewController *contactVC = (TBContactListViewController *)[[Manager sharedInstance] buildViewControllerFromStoryBoard:@"Main" withViewControllerId:@"TBContactListViewController"];
    contactVC.selectedItems = assets;
    contactVC.delegate = self;
    [self.navigationController pushViewController:contactVC animated:YES];
    assets = nil;
}

- (void)qb_imagePickerControllerDidCancel:(QBImagePickerController *)imagePickerController
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - Actions
- (IBAction)tbButtonTapped:(id)sender
{
    [self checkAddressbook];
}

- (IBAction)clearActivityLogTapped:(id)sender
{
    [AlertManager showAlertWithTitle:@"Are you sure that you want to delete all activity log?" message:@"" cancelButtonTitle:@"No" otherButtonTitles:@[@"Yes"] viewController:self completionHandler:^(NSInteger buttonClicked)
    {
        if (buttonClicked == 1)
        {
            self.recentActivityArray = @[].mutableCopy;
            [self.collectionView reloadData];
            self.activityLogLabel.text = NSLocalizedString(@"noActivityLog", nil);;
        }
    }];
}

- (IBAction)downloadListButtonTapped:(id)sender
{
    TBDownloadViewController *dlListVC = (TBDownloadViewController *)[[Manager sharedInstance] buildViewControllerFromStoryBoard:@"Main" withViewControllerId:@"TBDownloadViewController"];
    //UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:dlListVC];
    [self.navigationController pushViewController:dlListVC animated:YES];
}

- (IBAction)settingsButtonTapped:(id)sender
{
    TBSettingsViewController *setVC = (TBSettingsViewController *)[[Manager sharedInstance] buildViewControllerFromStoryBoard:@"Main" withViewControllerId:@"TBSettingsViewController"];
     UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:setVC];
    setVC.delegate = self;
    [self.navigationController presentViewController:navController animated:YES completion:nil];
}

#pragma mark - Download From Notification
- (void)downloadImagesFromNotification:(TBActivityLog *)activityLog
{
    TBDownloadViewController *dlListVC = (TBDownloadViewController *)[[Manager sharedInstance] buildViewControllerFromStoryBoard:@"Main" withViewControllerId:@"TBDownloadViewController"];
    dlListVC.activityFromNotification = activityLog;
    [self.navigationController pushViewController:dlListVC animated:YES];
}

#pragma mark - Check Addressbook
- (void)checkAddressbook
{
    __weak typeof(self) weakSelf = self;
    [AddressbookManager accessGrantedWithCompletionHandler:^(BOOL granted)
     {
         if (!granted)
         {
             [[Manager sharedInstance] addBlurEffectToView:self.view];
             [AlertManager showAlertWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"ContactsWarning", nil) cancelButtonTitle:NSLocalizedString(@"Cancel", nil) otherButtonTitles:@[NSLocalizedString(@"Settings", nil)] viewController:self completionHandler:^(NSInteger buttonClicked)
              {
                  if (buttonClicked == 0)
                  {
                      [[Manager sharedInstance] removeBlurEffect];
                  }
                  else
                  {
                      [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                  }
              }];
         }
         else
         {
             [[Manager sharedInstance] removeBlurEffect];
             // Open Image Picker
             QBImagePickerController *imagePickerController = [QBImagePickerController new];
             imagePickerController.delegate = weakSelf;
             imagePickerController.allowsMultipleSelection = YES;
             imagePickerController.maximumNumberOfSelection = kMaximumSelection;
             imagePickerController.showsNumberOfSelectedAssets = YES;
             imagePickerController.numberOfColumnsInPortrait = 5;
             [weakSelf presentViewController:imagePickerController animated:YES completion:nil];
         }
     }];
}

#pragma mark - SettingsVC
- (void)settingsVCLogoutButtonTapped
{
    [[Digits sharedInstance] logOut];
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - TDContactsListDelegate
- (void)contactListViewControllerDidSelectItem:(TBContactListViewController *)viewController
                             withRecieverUsers:(NSArray *)receivers
{
    self.tbButton.enabled = NO;
    [self.navigationController popToRootViewControllerAnimated:YES];
    self.uploadCollection = [NSMutableArray new];
    [self animateProgressView:YES withRecievers:receivers withSelectedItems:viewController.selectedItems];
}

#pragma mark -
- (void)animateProgressView:(BOOL)started
              withRecievers:(NSArray *)recievers
          withSelectedItems:(NSArray *)selectedItems
{
    __weak __typeof(self)weakSelf = self;
    if (started)
    {
        [UIView animateWithDuration:0.4f
                              delay:0.0f
                            options:UIViewAnimationOptionCurveEaseIn
                         animations:^{
                             weakSelf.activityLogTopConstraint.constant = kInProgressActvityLogConst;
                             ;
                             [weakSelf.view layoutIfNeeded];
                         } completion:^(BOOL finished) {
                             weakSelf.inProgressView.hidden = NO;
                             if (recievers.count == 1)
                             {
                                 weakSelf.recieverNameLabel.text = [NSString stringWithFormat:NSLocalizedString(@"toLabel", nil), ((TBUser *)[recievers firstObject]).userName];
                             }
                             else
                             {
                                 weakSelf.recieverNameLabel.text = [NSString stringWithFormat:NSLocalizedString(@"toLabelMultiple", nil), recievers.count];
                             }
                             weakSelf.uploadingLabel.text = [NSString stringWithFormat:NSLocalizedString(@"uploading", nil)];
                             [weakSelf startUploadWithRecievers:recievers withSelectedItems:selectedItems];
                         }];
    }
    else
    {
        [UIView animateWithDuration:0.4f
                              delay:0.0f
                            options:UIViewAnimationOptionCurveEaseIn
                         animations:^{
                             weakSelf.inProgressView.hidden = YES;
                             weakSelf.recieverNameLabel.text = @"";
                             weakSelf.collectionUploadCount = 0;
                             weakSelf.collectionUploadErrorCount = 0;
                             weakSelf.activityLogTopConstraint.constant = kActvityLogConst;
                             ;
                             [weakSelf.view layoutIfNeeded];
                         } completion:^(BOOL finished) {
                             [weakSelf fetchActivityLog:nil];
                             [weakSelf.circularUploadBarView setValue:0.0f];
                         }];
    }
}

- (void)startUploadWithRecievers:(NSArray *)recievers
               withSelectedItems:(NSArray *)selectedItems
{
    self.uniqueIDForTransfer = [[NSUUID UUID] UUIDString];
    self.currentActivity = [TBActivityLog new];
    self.currentActivity.directoryID = self.uniqueIDForTransfer;
    self.currentActivity.ownerName = [UserManager loadUser].userName;
    self.currentActivity.ownerPhone = [UserManager loadUser].phoneNumber;
    self.currentActivity.recievers = recievers.mutableCopy;
    self.currentActivity.numberOfPhotos = selectedItems.count;
    
    // Analytics
    NSMutableString *recieverPhones = @"".mutableCopy;
    for (NSInteger i = 0; i < recievers.count; i++)
    {
        [recieverPhones stringByAppendingString:[NSString stringWithFormat:@"%@,",[recievers objectAtIndex:i]]];
    }
    [AnalyticsManager trackEventWithAction:@"download"
                         withEventCategory:(recievers.count > 1) ? @"multiple upload" : @"single upload"
                            withEventLabel:[NSString stringWithFormat:@"from: %@ recievers: %@", [UserManager loadUser].phoneNumber, recieverPhones]];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    PHImageRequestOptions *requestOptions = [[PHImageRequestOptions alloc] init];
    requestOptions.version = PHImageRequestOptionsVersionUnadjusted;
    requestOptions.resizeMode   = PHImageRequestOptionsResizeModeFast;
    requestOptions.deliveryMode = PHImageRequestOptionsDeliveryModeFastFormat;
    [self requestImageForAssetWithAssetArray:selectedItems.mutableCopy withRequestOptions:requestOptions withRecievers:recievers];
}

- (void)requestImageForAssetWithAssetArray:(NSMutableArray *)assets withRequestOptions:(PHImageRequestOptions *)requestOptions withRecievers:(NSArray *)recievers
{
    if (assets.count == 0)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        });
        DebugLog(@"Requesting for all images finished");
    }
    else
    {
        @autoreleasepool
        {
            [[PHImageManager defaultManager] requestImageForAsset:[assets objectAtIndex:0]
                                                       targetSize:PHImageManagerMaximumSize
                                                      contentMode:PHImageContentModeDefault
                                                          options:requestOptions
                                                    resultHandler:^void(UIImage *image, NSDictionary *info)
             {
                 DebugLog(@"%ld", assets.count);
                 @autoreleasepool
                 {
                     if (image)
                     {
                         // Write image to system
                         NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                         NSString *fileName = [NSString stringWithFormat:@"%ld_%@.jpg", (long)assets.count, [NSDate date]];
                         NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:fileName];
                         NSData *imgData = UIImageJPEGRepresentation(image, 1);
                         if ([imgData length] > kAllowedByteSize)
                         {
                             [UIImageJPEGRepresentation(image, 0.65) writeToFile:filePath atomically:YES];
                         }
                         else
                         {
                             [imgData writeToFile:filePath atomically:YES];
                         }
                         
                         // Start AWSS3Transfer Request
                         AWSS3TransferManagerUploadRequest *uploadRequest = [AWSS3TransferManagerUploadRequest new];
                         uploadRequest.body = [NSURL fileURLWithPath:filePath];
                         uploadRequest.key = [NSString stringWithFormat:@"%@/%@", self.uniqueIDForTransfer, fileName];
                         uploadRequest.bucket = @"photopatchapp";
                         [self.uploadCollection insertObject:uploadRequest atIndex:0];
                         [self upload:uploadRequest withRecievers:recievers];
                     }
                     [assets removeObjectAtIndex:0];
                     [self requestImageForAssetWithAssetArray:assets withRequestOptions:requestOptions withRecievers:recievers];
                 }
             }];
        }
    }
}

#pragma mark - S3
- (void)upload:(AWSS3TransferManagerUploadRequest *)uploadRequest
 withRecievers:(NSArray *)recievers
{
    __weak typeof(self) weakSelf = self;
    AWSS3TransferUtility *transferUtility = [AWSS3TransferUtility defaultS3TransferUtility];
    [[transferUtility uploadFile:uploadRequest.body
                          bucket:@"photopatchapp"
                             key:uploadRequest.key
                     contentType:@"text/plain"
                      expression:nil
                completionHander:^(AWSS3TransferUtilityUploadTask * _Nonnull task, NSError * _Nullable error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        // Do something e.g. Alert a user for transfer completion.
                        // On failed uploads, `error` contains the error object.
                        __strong __typeof(weakSelf)strongSelf = weakSelf;
                        NSUInteger index = [strongSelf.uploadCollection indexOfObject:uploadRequest];
                        DebugLog(@"upload of file %ld finished.", (long)index);
                        if (error)
                        {
                            self.collectionUploadErrorCount++;
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [AlertManager showTSAlertWithTitle:NSLocalizedString(@"Error", nil) message:[NSString stringWithFormat:@"Upload File Error: %ld", (long)index] type:TSMessageNotificationTypeError];
                            });
                        }
                        strongSelf.collectionUploadCount++;
                        CGFloat progress = (CGFloat)((CGFloat)strongSelf.collectionUploadCount / (CGFloat)strongSelf.uploadCollection.count) * 100;
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [strongSelf.circularUploadBarView setValue:progress animateWithDuration:0.2f];
                        });
                        // Delete stored image from iphone again.
                        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                        NSString *realPath = [[uploadRequest.key componentsSeparatedByString:@"/"] lastObject];
                        NSString *absImgPath = [[paths objectAtIndex:0] stringByAppendingPathComponent:realPath];
                        [[NSFileManager defaultManager] removeItemAtPath:absImgPath error:nil];
                        
                        if (strongSelf.collectionUploadCount == (strongSelf.uploadCollection.count - strongSelf.collectionUploadErrorCount))
                        {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [strongSelf animateProgressView:NO withRecievers:recievers withSelectedItems:nil];
                            });
                            [strongSelf sendUploadFinishedFlag];
                        }
                    });
                }] continueWithBlock:^id(AWSTask *task){
        if (task.error)
        {
            self.collectionUploadErrorCount++;
            dispatch_async(dispatch_get_main_queue(), ^{
                [AlertManager showTSAlertWithTitle:NSLocalizedString(@"Error", nil) message:task.error.localizedDescription type:TSMessageNotificationTypeError];
            });
            
        }
        if (task.exception)
        {
            self.collectionUploadErrorCount++;
            dispatch_async(dispatch_get_main_queue(), ^{
                [AlertManager showTSAlertWithTitle:NSLocalizedString(@"Error", nil) message:[NSString stringWithFormat:@"%@", task.exception] type:TSMessageNotificationTypeError];
            });
        }
        if (task.result)
        {
            //AWSS3TransferUtilityUploadTask *uploadTask = task.result;
            // Do something with uploadTask.
        }
        
        return nil;
    }];
    
}

- (void)cancelAllUploads:(id)sender
{
    [self.uploadCollection enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)
    {
        if ([obj isKindOfClass:[AWSS3TransferManagerUploadRequest class]])
        {
            AWSS3TransferManagerUploadRequest *uploadRequest = obj;
            [[uploadRequest cancel] continueWithBlock:^id(AWSTask *task)
            {
                if (task.error)
                {
                    DebugLog(@"The cancel request failed: [%@]", task.error);
                }
                return nil;
            }];
        }
    }];
}

#pragma mark - Server
- (void)sendUploadFinishedFlag
{
    self.tbButton.enabled = YES;
    self.uploadCollection = [NSMutableArray new];
    [[NetworkManager sharedInstance] sendUploadFinishedFlagWithActivityLog:self.currentActivity withSuccessBlock:^(BOOL success)
    {
        [AlertManager showAlertWithTitle:NSLocalizedString(@"UploadSuccess", nil) message:nil cancelButtonTitle:NSLocalizedString(@"Done", nil) otherButtonTitles:nil viewController:self completionHandler:nil];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    } failureBlock:^(NSError *error) {
        [AlertManager showTSAlertWithTitle:NSLocalizedString(@"Error", nil) message:error.localizedDescription type:TSMessageNotificationTypeError];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
}

#pragma mark - Helpers
- (BOOL)arrayContainsUser:(NSArray *)recieverArray
{
    for (TBUser *user in recieverArray)
    {
        if ([user.phoneNumber isEqualToString:[UserManager loadUser].phoneNumber])
        {
            return YES;
        }
    }
    return NO;
}

- (BOOL)userDownloadedActivity:(NSArray *)recieverArray
{
    for (TBUser *user in recieverArray)
    {
        if ([user.phoneNumber isEqualToString:[UserManager loadUser].phoneNumber])
        {
            return user.downloaded;
        }
    }
    return NO;
}

@end
