//
//  TBTutorialViewController.h
//  Throwback
//
//  Created by Onur Şahindur on 13/07/16.
//  Copyright © 2016 Throwback Corp. All rights reserved.
//

#import "TBViewController.h"

@protocol TBTutorialViewControllerDelegate <NSObject>

- (void)tutorialViewControllerDidFinishTutorial;

@end

@interface TBTutorialViewController : TBViewController

@property (weak, nonatomic) id<TBTutorialViewControllerDelegate>delegate;

@end
