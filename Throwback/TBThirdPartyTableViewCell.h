//
//  TBThirdPartyTableViewCell.h
//  Throwback
//
//  Created by Onur Şahindur on 18/07/16.
//  Copyright © 2016 Throwback Corp. All rights reserved.
//

@interface TBThirdPartyTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel        *thirdPartyNameLabel;
@property (weak, nonatomic) IBOutlet UITextView     *thirdPartyValueLabel;
@property (weak, nonatomic) IBOutlet UIImageView    *arrowImageView;

@end
