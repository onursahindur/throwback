//
//  TBTutorialViewController.m
//  Throwback
//
//  Created by Onur Şahindur on 13/07/16.
//  Copyright © 2016 Throwback Corp. All rights reserved.
//

#import "TBTutorialViewController.h"

@interface TBTutorialViewController ()

@property (weak, nonatomic) IBOutlet UIImageView            *buttonImageView;
@property (weak, nonatomic) IBOutlet UILabel                *buttonLabel;
@property (weak, nonatomic) IBOutlet UIImageView            *dlManagerImageView;
@property (weak, nonatomic) IBOutlet UILabel                *dlManagerLabel;
@property (weak, nonatomic) IBOutlet UIImageView            *recentImageView;
@property (weak, nonatomic) IBOutlet UILabel                *recentLabel;
@property (weak, nonatomic) IBOutlet UIImageView            *settingsImageView;
@property (weak, nonatomic) IBOutlet UILabel                *settingsLabel;
@property (weak, nonatomic) IBOutlet UIPageControl          *pageControl;

@property (assign, nonatomic) NSInteger count;

@end

@implementation TBTutorialViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.count = 0;
    [self localizeLabels];
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapped)];
    [self.view addGestureRecognizer:tapGestureRecognizer];
}

- (void)localizeLabels
{
    self.buttonLabel.text = NSLocalizedString(@"buttonLabel", nil);
    self.dlManagerLabel.text = NSLocalizedString(@"dlManagerLabel", nil);
    self.recentLabel.text = NSLocalizedString(@"recentLabel", nil);
    self.settingsLabel.text = NSLocalizedString(@"settingsLabel", nil);
}

- (void)tapped
{
    self.count++;
    self.pageControl.currentPage = self.count;
    if (self.count == 1)
    {
        self.buttonLabel.hidden = YES;
        self.buttonImageView.hidden = YES;
        self.recentLabel.hidden = NO;
        self.recentImageView.hidden = NO;
    }
    if (self.count == 2)
    {
        self.recentLabel.hidden = YES;
        self.recentImageView.hidden = YES;
        self.dlManagerLabel.hidden = NO;
        self.dlManagerImageView.hidden = NO;
    }
    if (self.count == 3)
    {
        self.dlManagerLabel.hidden = YES;
        self.dlManagerImageView.hidden = YES;
        self.settingsLabel.hidden = NO;
        self.settingsImageView.hidden = NO;
    }
    if (self.count == 4)
    {
        [self dismissViewControllerAnimated:NO completion:nil];
        if ([self.delegate respondsToSelector:@selector(tutorialViewControllerDidFinishTutorial)])
        {
            [self.delegate tutorialViewControllerDidFinishTutorial];
        }
    }
}


@end
