//
//  TBContactListViewController.m
//  Throwback
//
//  Created by Onur Şahindur on 01/07/16.
//  Copyright © 2016 Throwback Corp. All rights reserved.
//

#import "TBContactListViewController.h"
#import "TBFriendCollectionViewCell.h"

static CGFloat      const kCellHeight           = 60.0f;
static NSInteger    const kAllowedUsers         = 7;

@interface TBContactListViewController () <UICollectionViewDelegate, UICollectionViewDataSource, UISearchBarDelegate>

@property (nonatomic, strong)   NSArray                     *dataSourceForSearchResult;
@property (nonatomic, strong)   NSMutableArray              *notTBContactArray;
@property (nonatomic, strong)   NSMutableArray              *selectedUsers;
@property (nonatomic, assign)   BOOL                        searchBarActive;
@property (nonatomic, assign)   BOOL                        animationActive;

@property (weak, nonatomic)     IBOutlet UISearchBar        *searchBar;
@property (weak, nonatomic)     IBOutlet UIButton           *doneButton;
@property (weak, nonatomic)     IBOutlet NSLayoutConstraint *doneButtonBottomConstraint;

@end

@implementation TBContactListViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initUI];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (![Manager sharedInstance].tbContactArray
        || [Manager sharedInstance].tbContactArray.count == 0)
    {
        [self loadAddressbook:nil];
    }
    [NavigationManager styleNavigationTitleWithText:@"#throwback" withViewController:self];
    [NavigationManager addRightBarButtonWithTitle:nil image:[UIImage imageNamed:@"refresh"] target:self selector:@selector(refreshContactList:) withFont:nil viewController:self];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [super viewDidDisappear:animated];
}

- (void)initUI
{
    [self.collectionView registerNib:[UINib nibWithNibName:@"TBFriendCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"TBFriendCollectionViewCell"];
    [self.collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind: UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView"];
    self.searchBar.placeholder = [NSString stringWithFormat:NSLocalizedString(@"searchBarPlaceholderFirst", nil)];
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refreshContactList:)
             forControlEvents:UIControlEventValueChanged];
    [self.collectionView addSubview:refreshControl];
    self.collectionView.alwaysBounceVertical = YES;
    self.selectedUsers = [NSMutableArray new];
    [self.doneButton setTitle:NSLocalizedString(@"Send", nil) forState:UIControlStateNormal];
    self.doneButton.enabled = NO;
    self.doneButton.alpha = 0.6f;
}

- (void)refreshContactList:(id)sender
{
    self.selectedUsers = [NSMutableArray new];
    self.doneButton.enabled = NO;
    self.doneButton.alpha = 0.6f;
    UIView *itemView = [self.navigationItem.rightBarButtonItem performSelector:@selector(view)];
    UIImageView *imageView = [itemView.subviews firstObject];
    imageView.contentMode = UIViewContentModeCenter;
    imageView.autoresizingMask = UIViewAutoresizingNone;
    imageView.clipsToBounds = NO;
    
    __weak typeof(self)weakSelf = self;
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^{
        imageView.transform = CGAffineTransformMakeRotation(M_PI);
        [weakSelf.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^{
            imageView.transform = CGAffineTransformMakeRotation(0);
            [weakSelf.view layoutIfNeeded];
        } completion:nil];
    }];
    [self loadAddressbook:sender];
}

- (void)loadAddressbook:(id)sender
{
    __weak typeof(self)weakSelf = self;
    [Manager sharedInstance].tbContactArray = [NSMutableArray new];
    [self.collectionView reloadData];

    if (![sender isKindOfClass:[UIRefreshControl class]]) [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [AddressbookManager getContactNamesWithViewController:self withCompletionHandler:^(NSArray *contacts, NSArray *phoneNumbers)
    {
        NSMutableArray *allUsers = [NSMutableArray new];
        for (NSInteger i = 0; i < contacts.count; i++)
        {
            TBUser *contact = [TBUser new];
            contact.userName = [contacts objectAtIndex:i];
            contact.phoneNumber = [phoneNumbers objectAtIndex:i];
            [allUsers addObject:contact];
        }
        
        [[NetworkManager sharedInstance] userCheckContactsWithContacts:phoneNumbers.mutableCopy withSuccessBlock:^(NSArray *contacts)
        {
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            [MBProgressHUD hideHUDForView:strongSelf.view animated:YES];
            if (![sender isKindOfClass:[UIRefreshControl class]])
            {
                [MBProgressHUD hideHUDForView:strongSelf.view animated:YES];
            }
            else
            {
                [(UIRefreshControl *)sender endRefreshing];
            }
            
//            strongSelf.notTBContactArray = [NSMutableArray new];
            for (TBUser *user in allUsers)
            {
                for (TBUser *tbUser in contacts)
                {
                    if ([user.phoneNumber isEqualToString:tbUser.phoneNumber])
                    {
                        [[Manager sharedInstance].tbContactArray addObject:user];
                    }
//                    else
//                    {
//                        [strongSelf.notTBContactArray addObject:user];
//                    }
                }
//                if ([Manager sharedInstance].tbContactArray.count == contacts.count)
//                    break; // Performance
            }
            NSSortDescriptor *sortDescriptor =[[NSSortDescriptor alloc] initWithKey:@"userName" ascending:YES];
            NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
            [Manager sharedInstance].tbContactArray = [[Manager sharedInstance].tbContactArray sortedArrayUsingDescriptors:sortDescriptors].mutableCopy;
//            strongSelf.notTBContactArray = [strongSelf.notTBContactArray sortedArrayUsingDescriptors:sortDescriptors].mutableCopy;
            [strongSelf.collectionView reloadData];
        } failureBlock:^(NSError *error) {
            [MBProgressHUD hideHUDForView:weakSelf.view animated:YES];
            if (![sender isKindOfClass:[UIRefreshControl class]])
            {
                [MBProgressHUD hideHUDForView:weakSelf.view animated:YES];
            }
            else
            {
                [(UIRefreshControl *)sender endRefreshing];
            }
            [AlertManager showTSAlertWithTitle:NSLocalizedString(@"Error", nil) message:error.localizedDescription type:TSMessageNotificationTypeError];
        }];
    }];
}

#pragma mark - Collection View
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (self.searchBarActive)
    {
        return self.dataSourceForSearchResult.count;
    }
    else
    {
        
//        if (section == 0)
//        {
            return [Manager sharedInstance].tbContactArray.count;
//        }
//        return self.notTBContactArray.count;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    TBFriendCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TBFriendCollectionViewCell" forIndexPath:indexPath];
    TBUser *user;
    if (self.searchBarActive)
    {
        user = [self.dataSourceForSearchResult objectAtIndex:indexPath.row];
    }
    else
    {
//        if (indexPath.section == 0)
//        {
            user = [[Manager sharedInstance].tbContactArray objectAtIndex:indexPath.row];
//        }
//        else
//        {
//            user = [self.notTBContactArray objectAtIndex:indexPath.row];
//        }
    }
    
    if ([self.selectedUsers containsObject:user])
    {
        [cell.circularView setValue:100.0f];
        [cell.contentView setBackgroundColor:[UIColor lightGrayColor]];
        cell.hashtagLabel.hidden = NO;
    }
    else
    {
        [cell.circularView setValue:0.f];
        [cell.contentView setBackgroundColor:[UIColor clearColor]];
        cell.hashtagLabel.hidden = YES;
    }
    
    cell.contactNameLabel.text = user.userName;
    cell.phoneNumberLabel.text = user.phoneNumber;
    return cell;
}

//- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    if (indexPath.section == 1) NO;
//    return YES;
//}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    TBFriendCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TBFriendCollectionViewCell" forIndexPath:indexPath];
    TBUser *user;
    if (self.searchBarActive)
    {
        user = [self.dataSourceForSearchResult objectAtIndex:indexPath.row];
    }
    else
    {
        //        if (indexPath.section == 0)
        //        {
        user = [[Manager sharedInstance].tbContactArray objectAtIndex:indexPath.row];
        //        }
        //        else
        //        {
        //            user = [self.notTBContactArray objectAtIndex:indexPath.row];
        //        }
    }
    if ([self.selectedUsers containsObject:user])
    {
        [self.selectedUsers removeObject:user];
        [cell.circularView setValue:0.f animateWithDuration:0.5f];
    }
    else
    {
        if (self.selectedUsers.count < 7)
        {
            [self.selectedUsers addObject:user];
            [cell.circularView setValue:100.f animateWithDuration:0.5f];
        }
        else
        {
            [AlertManager showAlertWithTitle:NSLocalizedString(@"Error", nil)  message:[NSString stringWithFormat:NSLocalizedString(@"allowedUserCount", nil), kAllowedUsers] cancelButtonTitle:@"OK" otherButtonTitles:nil viewController:self completionHandler:nil];
        }
    }
    if (self.selectedUsers.count > 0)
    {
        self.doneButton.enabled = YES;
        self.doneButton.alpha = 1.0f;
    }
    else
    {
        self.doneButton.enabled = NO;
        self.doneButton.alpha = 0.6f;
    }
    [self.collectionView reloadItemsAtIndexPaths:@[indexPath]];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(CGRectGetWidth(self.view.frame), kCellHeight);
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if (kind == UICollectionElementKindSectionHeader)
    {
        UICollectionReusableView *reusableview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView" forIndexPath:indexPath];
        
        if (reusableview == nil)
        {
            reusableview = [[UICollectionReusableView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), 30)];
        }
        
        reusableview.backgroundColor = TB_BACKGROUND;
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, CGRectGetWidth(self.view.frame), 30)];
        [label setFont:[UIFont italicSystemFontOfSize:12]];
        label.textColor = TB_BLUE;
        
        if (indexPath.section == 0)
        {
            label.text = NSLocalizedString(@"usingTB", nil);
        }
        else
        {
            label.text = NSLocalizedString(@"notUsingTB", nil);
        }
        
        [reusableview addSubview:label];
        return reusableview;
    }
    return nil;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    return CGSizeMake(CGRectGetWidth(self.view.frame), 30);
}

#pragma mark - SearchBar Delegate
- (void)filterContentForSearchText:(NSString*)searchText
{
    NSPredicate *resultPredicate    = [NSPredicate predicateWithFormat:@"self.userName contains[c] %@", searchText];
    self.dataSourceForSearchResult  = [[Manager sharedInstance].tbContactArray filteredArrayUsingPredicate:resultPredicate];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (searchText.length > 0)
    {
        self.searchBarActive = YES;
        [self filterContentForSearchText:searchText];
        [self.collectionView reloadData];
    }
    else
    {
        self.searchBarActive = NO;
    }
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self cancelSearching];
    [self.collectionView reloadData];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    self.searchBarActive = YES;
    [self.view endEditing:YES];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [self.searchBar setShowsCancelButton:YES animated:YES];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    self.searchBarActive = NO;
    [self.searchBar setShowsCancelButton:NO animated:YES];
}

- (void)cancelSearching
{
    self.searchBarActive = NO;
    [self.searchBar resignFirstResponder];
    self.searchBar.text  = @"";
}
#pragma mark - keyboard
- (void)keyboardWillShow:(NSNotification *)notification
{
    CGFloat keyboardHeight = CGRectGetHeight([notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue]);
    __weak __typeof(self)weakSelf = self;
    [UIView animateWithDuration:0.4f
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         weakSelf.doneButtonBottomConstraint.constant = keyboardHeight;
                         [weakSelf.view layoutIfNeeded];
                     } completion:nil];
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    __weak __typeof(self)weakSelf = self;
    [UIView animateWithDuration:0.4f
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         weakSelf.doneButtonBottomConstraint.constant = 0.0f;
                         [weakSelf.view layoutIfNeeded];
                     } completion:nil];
}

- (IBAction)doneButtonTapped:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(contactListViewControllerDidSelectItem:withRecieverUsers:)])
    {
        NSString *title = @"";
        if ([self.selectedUsers count] == 1)
        {
            title = [NSString stringWithFormat:NSLocalizedString(@"SendSure", nil), ((TBUser *)[self.selectedUsers firstObject]).userName, (long)self.selectedItems.count];
        }
        else
        {
            title = [NSString stringWithFormat:NSLocalizedString(@"SendSureMultiple", nil), (long)self.selectedItems.count, (long)self.selectedUsers.count];
        }
        [AlertManager showAlertWithTitle:title message:nil cancelButtonTitle:NSLocalizedString(@"Cancel", nil) otherButtonTitles:@[NSLocalizedString(@"Send", nil)] viewController:self completionHandler:^(NSInteger buttonClicked) {
            if (buttonClicked == 1)
            {
                [self.delegate contactListViewControllerDidSelectItem:self withRecieverUsers:self.selectedUsers];
            }
        }];
    }
    else
    {
        [AlertManager showAlertWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"WentWrong", nil) cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil viewController:self completionHandler:nil];
    }
    
}

@end
