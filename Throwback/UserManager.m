//
//  UserManager.m
//  Throwback
//
//  Created by Onur Şahindur on 03/07/16.
//  Copyright © 2016 Throwback Corp. All rights reserved.
//

#import "UserManager.h"

@implementation UserManager

+ (BOOL)saveUser:(TBUser *)user
{
    NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:user];
    [[NSUserDefaults standardUserDefaults] setObject:encodedObject forKey:@"savedUser"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    return YES;
}

+ (TBUser *)loadUser
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *encodedObject = [defaults objectForKey:@"savedUser"];
    return (TBUser *)[NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
}

+ (BOOL)clearSavedUser
{
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"savedUser"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    return YES;
}

@end
