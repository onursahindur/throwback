//
//  TBDownloadViewController.m
//  Throwback
//
//  Created by Onur Şahindur on 09/07/16.
//  Copyright © 2016 Throwback Corp. All rights reserved.
//

static CGFloat      const kCellHeight                       = 70.0f;

#import "TBDownloadViewController.h"
#import "TBSentCollectionViewCell.h"
#import "TBActivityLog.h"
#import <AWSS3/AWSS3.h>
#import "MBCircularProgressBarView.h"
#import "NYTPhotosViewController.h"
#import "NYTPhotosOverlayView.h"
#import "TBPhoto.h"
#import <Photos/Photos.h>

@interface TBDownloadViewController () <UICollectionViewDelegate, UICollectionViewDataSource,
                                            TBSentCollectionViewCellDelegate>

@property (weak, nonatomic) IBOutlet UIView                     *downloadProcessView;
@property (weak, nonatomic) IBOutlet UILabel                    *downloadInProcessLabel;
@property (weak, nonatomic) IBOutlet UILabel                    *fromLabel;
@property (weak, nonatomic) IBOutlet MBCircularProgressBarView  *circularProgressView;

@property (nonatomic, strong) NSMutableArray                    *downloadListArray;
@property (nonatomic, strong) NSMutableArray                    *s3ImageCollectionArray;
@property (nonatomic, strong) NSMutableArray                    *downloadedImages;
@property (nonatomic, assign) NSInteger                         numberOfFiles;
@property (nonatomic, strong) TBActivityLog                     *currentActivity;

@end

@implementation TBDownloadViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initUI];
    [self getDownloadList:nil];
    [AnalyticsManager trackScreenWithName:@"Download Manager"];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.activityFromNotification)
    {
        [self askDownloadRequest];
    }
}

- (void)askDownloadRequest
{
    __weak __typeof(self)weakSelf = self;
    [AlertManager showAlertWithTitle:NSLocalizedString(@"DownloadNow", nil) message:nil cancelButtonTitle:NSLocalizedString(@"Later", nil) otherButtonTitles:@[NSLocalizedString(@"Download", nil)] viewController:self completionHandler:^(NSInteger buttonClicked) {
        
        __strong __typeof(weakSelf)strongSelf = weakSelf;
        if (buttonClicked == 1)
        {
            [strongSelf downloadImagesWitActivity:strongSelf.activityFromNotification];
        }
        else
        {
            [strongSelf getDownloadList:nil];
        }
    }];
}

- (void)initUI
{
    [self.collectionView registerNib:[UINib nibWithNibName:@"TBSentCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"TBSentCollectionViewCell"];
    self.navigationController.navigationBar.translucent = NO;
    //[NavigationManager addRightBarButtonWithTitle:NSLocalizedString(@"Close", nil) image:nil target:self selector:@selector(closeButtonTapped:) withFont:nil viewController:self];
    [NavigationManager styleNavigationTitleWithText:NSLocalizedString(@"downloadList", nil) withViewController:self];
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(getDownloadList:)
             forControlEvents:UIControlEventValueChanged];
    [self.collectionView addSubview:refreshControl];
    self.collectionView.alwaysBounceVertical = YES;
}

- (void)getDownloadList:(id)sender
{
    __weak typeof(self) weakSelf = self;
    if (!sender) [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[NetworkManager sharedInstance] getDownloadListWithUser:[UserManager loadUser] withSuccessBlock:^(NSArray *downloadActivityArray) {
        __strong __typeof(weakSelf)strongSelf = weakSelf;
        if (!sender)
        {
            [MBProgressHUD hideHUDForView:strongSelf.view animated:YES];
        }
        else
        {
            [(UIRefreshControl *)sender endRefreshing];
        }
        strongSelf.downloadListArray = downloadActivityArray.mutableCopy;
        [strongSelf.collectionView reloadData];
    } failureBlock:^(NSError *error) {
        if (!sender)
        {
            [MBProgressHUD hideHUDForView:weakSelf.view animated:YES];
        }
        else
        {
            [(UIRefreshControl *)sender endRefreshing];
        }
        [AlertManager showTSAlertWithTitle:NSLocalizedString(@"Error", nil) message:error.localizedDescription type:TSMessageNotificationTypeError];
    }];
}

#pragma mark - Collection View
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.downloadListArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    TBSentCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TBSentCollectionViewCell" forIndexPath:indexPath];
    TBActivityLog *log = [self.downloadListArray objectAtIndex:indexPath.row];
    cell.delegate = self;
    cell.activity = log;
    [cell.contentView setBackgroundColor:[UIColor clearColor]];
    cell.sentUserNameLabel.text = [NSString stringWithFormat:NSLocalizedString(@"fromName", nil), log.ownerName];
    [cell.statusImageView setImage:[UIImage imageNamed:@"recieved"]];
    cell.photoCountLabel.text = [NSString stringWithFormat:NSLocalizedString(@"photoCount", nil), (long)log.numberOfPhotos];
    if (log.logType == TBActivityLogTypeUploaded)
    {
        cell.statusLabel.textColor = TB_ORANGE;
        cell.statusLabel.text = NSLocalizedString(@"uploaded", nil);
    }
    else if (log.logType == TBActivityLogTypeDownloaded)
    {
        cell.statusLabel.textColor = RGB(0, 90, 90);
        cell.statusLabel.text = NSLocalizedString(@"downloaded", nil);
    }
    else if (log.logType == TBActivityLogTypeCancelled)
    {
        cell.statusLabel.textColor = [UIColor redColor];
        cell.statusLabel.text = NSLocalizedString(@"cancelled", nil);
    }
    cell.timeLabel.text = log.shortWhenAnnotation;
    cell.statusImageView.image = [cell.statusImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    //
    TBActivityLog *activity = [self.downloadListArray objectAtIndex:indexPath.row];
    __weak typeof(self) weakSelf = self;
    [AlertManager showAlertWithTitle:[NSString stringWithFormat:NSLocalizedString(@"SureDownload", nil), activity.numberOfPhotos] message:nil cancelButtonTitle:NSLocalizedString(@"Cancel", nil) otherButtonTitles:@[NSLocalizedString(@"Download", nil)] viewController:self completionHandler:^(NSInteger buttonClicked) {
        __strong __typeof(weakSelf)strongSelf = weakSelf;
        if (buttonClicked == 1)
        {
            [strongSelf downloadImagesWitActivity:activity];
        }
    }];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(CGRectGetWidth(self.view.frame), kCellHeight);
}

//#pragma mark - Actions
//- (void)closeButtonTapped:(id)sender
//{
//    [self dismissViewControllerAnimated:YES completion:nil];
//}

#pragma mark - TBSentCollectionViewDelegate
- (void)sentCollectionViewCellDeleteButtonTapped:(TBSentCollectionViewCell *)cell
                                    withActivity:(TBActivityLog *)activity
{
    for (TBActivityLog *log in self.downloadListArray)
    {
        if (log.ID == activity.ID)
        {
            [self.downloadListArray removeObject:log];
            break;
        }
    }
    [self.collectionView reloadData];
    [[NetworkManager sharedInstance] deleteSelectedActivityWithActivity:activity withSuccessBlock:^(BOOL success) {
        // Nothing to do here...
    } failureBlock:^(NSError *error) {
        DebugLog(@"%@", error.localizedDescription);
    }];
}

#pragma mark - Download progress
- (void)downloadImagesWitActivity:(TBActivityLog *)activity
{
    // Analytics
    [AnalyticsManager trackEventWithAction:@"download"
                         withEventCategory:(self.activityFromNotification) ? @"notification" : @"normal"
                            withEventLabel:[NSString stringWithFormat:@"from: %@ to: %@", activity.ownerPhone, [UserManager loadUser].phoneNumber]];
    
    self.currentActivity = activity;
    self.activityFromNotification = nil;
    [[Manager sharedInstance] addBlurEffectToView:self.view];
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:1.0f
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         weakSelf.downloadProcessView.hidden = NO;
                         weakSelf.downloadProcessView.layer.zPosition = 1;
                         weakSelf.fromLabel.text = [NSString stringWithFormat:NSLocalizedString(@"fromName", nil), weakSelf.currentActivity.ownerName];
                         weakSelf.downloadInProcessLabel.text = NSLocalizedString(@"downloadInProcess", nil);
                         weakSelf.navigationItem.hidesBackButton = YES;
                         [weakSelf.view bringSubviewToFront:weakSelf.downloadProcessView];
                         [weakSelf.view layoutIfNeeded];
                     } completion:^(BOOL finished) {
                         weakSelf.s3ImageCollectionArray = [NSMutableArray new];
                         weakSelf.downloadedImages = [NSMutableArray new];
                         [weakSelf listObjectsWithWitActivity:activity];
                     }];
}

- (void)listObjectsWithWitActivity:(TBActivityLog *)activity
{
    AWSS3ListObjectsRequest *listObjectsRequest = [AWSS3ListObjectsRequest new];
    listObjectsRequest.bucket = @"photopatchapp";
    __weak typeof(self) weakSelf = self;
    [[[AWSS3 defaultS3] listObjects:listObjectsRequest] continueWithBlock:^id(AWSTask *task)
     {
         __strong __typeof(weakSelf)strongSelf = weakSelf;
         if (task.error)
         {
             DebugLog(@"listObjects failed: [%@]", task.error);
         }
         else
         {
             AWSS3ListObjectsOutput *listObjectsOutput = task.result;
             for (AWSS3Object *s3Object in listObjectsOutput.contents)
             {
                 if ([[[s3Object.key componentsSeparatedByString:@"/"] firstObject] isEqualToString:activity.directoryID])
                 {
                     // Create Path
                     NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                     NSString *fileName = [NSString stringWithFormat:@"%@_%@", [[s3Object.key componentsSeparatedByString:@"/"] firstObject], [[s3Object.key componentsSeparatedByString:@"/"] lastObject]];
                     NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:fileName];
                     
                     NSURL *downloadingFileURL = [NSURL fileURLWithPath:filePath];
                     AWSS3TransferManagerDownloadRequest *downloadRequest = [AWSS3TransferManagerDownloadRequest new];
                     downloadRequest.bucket = @"photopatchapp";
                     downloadRequest.key = s3Object.key;
                     downloadRequest.downloadingFileURL = downloadingFileURL;
                     [strongSelf.s3ImageCollectionArray addObject:downloadRequest];
                 }
             }
             
             // Start downloading first object
             [strongSelf download:[strongSelf.s3ImageCollectionArray firstObject]];
         }
         return nil;
     }];
}

- (void)download:(AWSS3TransferManagerDownloadRequest *)downloadRequest
{
    __weak typeof(self) weakSelf = self;
    AWSS3TransferUtility *transferUtility = [AWSS3TransferUtility defaultS3TransferUtility];
    [[transferUtility downloadToURL:downloadRequest.downloadingFileURL
                             bucket:downloadRequest.bucket
                                key:downloadRequest.key
                         expression:nil
                   completionHander:^(AWSS3TransferUtilityDownloadTask * _Nonnull task, NSURL * _Nullable location, NSData * _Nullable data, NSError * _Nullable error) {
                       __strong __typeof(weakSelf)strongSelf = weakSelf;
                       NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                       NSString *fileName = [NSString stringWithFormat:@"%@_%@", [[downloadRequest.key componentsSeparatedByString:@"/"] firstObject], [[downloadRequest.key componentsSeparatedByString:@"/"] lastObject]];
                       NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:fileName];
                       UIImage *image = [UIImage imageWithContentsOfFile:filePath];
//                       UIImageWriteToSavedPhotosAlbum(image ,
//                                                      strongSelf, // send the message to 'self' when calling the callback
//                                                      @selector(image:didFinishSavingWithError:contextInfo:), // the selector to tell the method to call on completion
//                                                      nil);
                       UIGraphicsBeginImageContext(image.size);
                       [image drawInRect:CGRectMake(0, 0, image.size.width, image.size.height)];
                       UIImage *copiedImage = UIGraphicsGetImageFromCurrentImageContext();
                       UIGraphicsEndImageContext();
                       TBPhoto *photo = [TBPhoto new];
                       photo.image = copiedImage;
                       [strongSelf.downloadedImages addObject:photo];
                       
                       [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^
                        {
                            //Checks for App Photo Album and creates it if it doesn't exist
                            PHFetchOptions *fetchOptions = [PHFetchOptions new];
                            fetchOptions.predicate = [NSPredicate predicateWithFormat:@"title == %@", @"#throwback"];
                            PHFetchResult *fetchResult = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeAlbum subtype:PHAssetCollectionSubtypeAlbumRegular options:fetchOptions];
                            
                            if (fetchResult.count == 0)
                            {
                                //Create Album
                                PHAssetCollectionChangeRequest *albumRequest = [PHAssetCollectionChangeRequest creationRequestForAssetCollectionWithTitle:@"#throwback"];
                            }
                            
                        } completionHandler:^(BOOL success, NSError *error) {
                            if (success) {
                                PHFetchOptions *fetchOptions = [PHFetchOptions new];
                                fetchOptions.predicate = [NSPredicate predicateWithFormat:@"title == %@", @"#throwback"];
                                PHFetchResult *fetchResult = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeAlbum subtype:PHAssetCollectionSubtypeAlbumRegular options:fetchOptions];
                                PHAssetCollection *assetCollection = fetchResult.firstObject;
                                
                                // Add it to the photo library
                                [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
                                    PHAssetChangeRequest *assetChangeRequest = [PHAssetChangeRequest creationRequestForAssetFromImage:image];
                                    PHAssetCollectionChangeRequest *assetCollectionChangeRequest = [PHAssetCollectionChangeRequest changeRequestForAssetCollection:assetCollection];
                                    [assetCollectionChangeRequest addAssets:@[[assetChangeRequest placeholderForCreatedAsset]]];
                                } completionHandler:^(BOOL success, NSError *error) {
                                    if (!success)
                                    {
                                        NSLog(@"Error creating asset: %@", error);
                                    }
                                    else
                                    {
                                        NSError *error = nil;
                                        BOOL success = [[NSFileManager defaultManager] removeItemAtPath:filePath error:&error];;
                                        if (!success || error)
                                        {
                                            DebugLog(@"Something went wrong...");
                                        }
                                    }
                                }];
                            }
                        }];
                       
                       [strongSelf.s3ImageCollectionArray removeObjectAtIndex:0];
                       dispatch_async(dispatch_get_main_queue(), ^{
                           CGFloat progress = (CGFloat)((CGFloat)(strongSelf.currentActivity.numberOfPhotos - strongSelf.s3ImageCollectionArray.count) / (CGFloat)strongSelf.currentActivity.numberOfPhotos) * 100;
                           [strongSelf.circularProgressView setValue:progress animateWithDuration:0.2f];
                       });
                       
                       if (strongSelf.s3ImageCollectionArray.count != 0)
                       {
                           [strongSelf download:[strongSelf.s3ImageCollectionArray firstObject]];
                       }
                       else
                       {
                           [strongSelf downloadComplete];
                       }
                   }] continueWithBlock:^id(AWSTask *task) {
                       if (task.error)
                       {
                           dispatch_async(dispatch_get_main_queue(), ^{
                               [AlertManager showTSAlertWithTitle:NSLocalizedString(@"Error", nil) message:task.error.localizedDescription type:TSMessageNotificationTypeError];
                               [[Manager sharedInstance] removeBlurEffect];
                               self.downloadProcessView.hidden = YES;
                               [self.circularProgressView setValue:0.0f];
                               self.navigationItem.hidesBackButton = NO;
                           });
                       }
                       if (task.exception)
                       {
                           dispatch_async(dispatch_get_main_queue(), ^{
                               [AlertManager showTSAlertWithTitle:NSLocalizedString(@"Error", nil) message:[NSString stringWithFormat:@"%@", task.exception] type:TSMessageNotificationTypeError];
                               [[Manager sharedInstance] removeBlurEffect];
                               self.downloadProcessView.hidden = YES;
                               [self.circularProgressView setValue:0.0f];
                               self.navigationItem.hidesBackButton = NO;
                           });
                       }
                       if (task.result)
                       {
                           AWSS3TransferUtilityDownloadTask *downloadTask = task.result;
                           // Do something with downloadTask.
                       }
                       
                       return nil;
    }];
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo;
{
    if (error)
    {
        // Do anything needed to handle the error or display it to the user
    }
    else
    {
        // .... do anything you want here to handle
        // .... when the image has been saved in the photo album
        // [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
    }
}

- (void)downloadComplete
{
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [weakSelf sendDownloadFinishedFlag];
        [AlertManager showAlertWithTitle:nil message:NSLocalizedString(@"DownloadSuccess", nil) cancelButtonTitle:NSLocalizedString(@"Done", nil) otherButtonTitles:@[NSLocalizedString(@"SeePhotos", nil)] viewController:[[Manager sharedInstance] topMostController] completionHandler:^(NSInteger buttonClicked)
         {
             __strong __typeof(weakSelf)strongSelf = weakSelf;
             [[Manager sharedInstance] removeBlurEffect];
             [strongSelf deleteTempFiles];
             strongSelf.downloadProcessView.hidden = YES;
             [strongSelf.circularProgressView setValue:0.0f];
             strongSelf.navigationItem.hidesBackButton = NO;
             if (buttonClicked == 1)
             {
                 NYTPhotosViewController *photosViewController = [[NYTPhotosViewController alloc] initWithPhotos:strongSelf.downloadedImages];
                 [photosViewController.overlayView addFromName:[NSString stringWithFormat:NSLocalizedString(@"fromName", nil), weakSelf.currentActivity.ownerName]];
                 [strongSelf presentViewController:photosViewController animated:YES completion:nil];
             }
         }];
    });
}

- (void)sendDownloadFinishedFlag
{
    __weak typeof(self) weakSelf = self;
    [[NetworkManager sharedInstance] sendDownloadFinishedFlagWithActivityLog:self.currentActivity withSuccessBlock:^(BOOL success) {
        [weakSelf getDownloadList:nil];
    } failureBlock:^(NSError *error) {
        [weakSelf sendDownloadFinishedFlag];
    }];
}

- (void)deleteTempFiles
{
    // delete all temp files
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSError *error = nil;
    for (NSString *file in [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[paths firstObject] error:&error])
    {
        NSString *absImgPath = [[paths objectAtIndex:0] stringByAppendingPathComponent:file];
        BOOL success = [[NSFileManager defaultManager] removeItemAtPath:absImgPath error:&error];;
        if (!success || error)
        {
            DebugLog(@"Something went wrong...");
        }
    }
}

@end
