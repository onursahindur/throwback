//
//  TBUsersDownloadedViewController.m
//  Throwback
//
//  Created by Onur Şahindur on 15/07/16.
//  Copyright © 2016 Throwback Corp. All rights reserved.
//

#import "TBUsersDownloadedViewController.h"
#import "TBUserTableViewCell.h"

@interface TBUsersDownloadedViewController () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewVeriticalConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *frameViewVerticalConstraint;

@end

@implementation TBUsersDownloadedViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapped:)];
    [self.view addGestureRecognizer:tapGesture];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.tableView registerNib:[UINib nibWithNibName:@"TBUserTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"TBUserTableViewCell"];
}

- (void)tapped:(id)sender
{
    __weak __typeof(self)weakSelf = self;
    [UIView animateWithDuration:0.1f
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         weakSelf.tableViewVeriticalConstraint.constant = -100;
                         ;
                         weakSelf.frameViewVerticalConstraint.constant = -100;
                         [weakSelf.view layoutIfNeeded];
                     } completion:^(BOOL finished) {
                         [weakSelf dismissViewControllerAnimated:YES completion:nil];
                     }];
    
}

#pragma mark - TableViewDataSource&Delegate
#pragma mark - TableView methods
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 35.0f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.recieverArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TBUserTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TBUserTableViewCell" forIndexPath:indexPath];
    cell.userNameLabel.text = ((TBUser *)[self.recieverArray objectAtIndex:indexPath.row]).userName;
    if (((TBUser *)[self.recieverArray objectAtIndex:indexPath.row]).downloaded)
    {
        [cell.userNameLabel setTextColor:RGB(0, 90, 90)];
        [cell.statusImageView setImage:[UIImage imageNamed:@"done"]];
    }
    else
    {
        [cell.userNameLabel setTextColor:TB_ORANGE];
        [cell.statusImageView setImage:[UIImage imageNamed:@"waiting"]];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Nothing to do here...
}

@end
