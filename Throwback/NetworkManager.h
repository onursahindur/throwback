//
//  NetworkManager.h
//  Throwback
//
//  Created by Onur Şahindur on 07/07/16.
//  Copyright © 2016 Throwback Corp. All rights reserved.
//

#import "AFNetworking.h"

typedef enum
{
    VersionTypeNone,
    VersionTypeUpdate,
    VersionTypeForceUpdate
}VersionType;

@interface NetworkManager : AFHTTPRequestOperationManager

typedef void (^FailureBlock)(NSError *error);

/**
 Returns the singleton shared instance.
 @return Singleton object.
 */
+ (instancetype)sharedInstance;

/**
 Cancels all the requests in the operation queue of the CBNetworkManager.
 @return void
 */
- (void)cancelRunningServices;

- (void)checkVersionWithVersion:(NSString *)version
               withSuccessBlock:(void (^)(VersionType))successBlock
                   failureBlock:(FailureBlock)failureBlock;

- (void)checkServerStateWithSuccessBlock:(void (^)(BOOL))successBlock
                            failureBlock:(FailureBlock)failureBlock;

- (void)registerUserWithUser:(TBUser *)user
            withSuccessBlock:(void (^)(TBUser *))successBlock
                failureBlock:(FailureBlock)failureBlock;

- (void)authenticateWithUser:(TBUser *)user
            withSuccessBlock:(void (^)(NSString *))successBlock
                failureBlock:(FailureBlock)failureBlock;

- (void)updateUser:(TBUser *)user
  withSuccessBlock:(void (^)(BOOL))successBlock
      failureBlock:(FailureBlock)failureBlock;

- (void)sendUploadFinishedFlagWithActivityLog:(TBActivityLog *)activity
                             withSuccessBlock:(void (^)(BOOL))successBlock
                                 failureBlock:(FailureBlock)failureBlock;

- (void)sendDownloadFinishedFlagWithActivityLog:(TBActivityLog *)activity
                               withSuccessBlock:(void (^)(BOOL))successBlock
                                   failureBlock:(FailureBlock)failureBlock;

- (void)getDownloadListWithUser:(TBUser *)user
               withSuccessBlock:(void (^)(NSArray *))successBlock
                   failureBlock:(FailureBlock)failureBlock;

- (void)getRecentActivitiesWithUser:(TBUser *)user
                   withSuccessBlock:(void (^)(NSArray *))successBlock
                       failureBlock:(FailureBlock)failureBlock;

- (void)deleteSelectedActivityWithActivity:(TBActivityLog *)activity
                          withSuccessBlock:(void (^)(BOOL))successBlock
                              failureBlock:(FailureBlock)failureBlock;

- (void)userCheckContactsWithContacts:(NSMutableArray *)contactPhones
                     withSuccessBlock:(void (^)(NSArray *))successBlock
                         failureBlock:(FailureBlock)failureBlock;

- (void)checkActivityStatus:(NSString *)activityID
           withSuccessBlock:(void (^)(NSString *))successBlock
               failureBlock:(FailureBlock)failureBlock;

- (void)logoutWithSuccessBlock:(void (^)(BOOL))successBlock
                  failureBlock:(FailureBlock)failureBlock;



@end
