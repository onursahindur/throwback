//
//  TBActivityLog.h
//  Throwback
//
//  Created by Onur Şahindur on 08/07/16.
//  Copyright © 2016 Throwback Corp. All rights reserved.
//

typedef enum
{
    TBActivityLogTypeUploaded,
    TBActivityLogTypeDownloaded,
    TBActivityLogTypeCancelled
}TBActivityLogType;

@interface TBActivityLog : NSObject

@property (nonatomic, strong) NSString              *ID;
@property (nonatomic, strong) NSString              *directoryID;
@property (nonatomic, strong) NSString              *ownerName;
@property (nonatomic, strong) NSString              *ownerPhone;
@property (nonatomic, strong) NSMutableArray        *recievers;
@property (nonatomic, assign) NSInteger             numberOfPhotos;
@property (nonatomic, strong) NSString              *uploadedTime;
@property (nonatomic, assign) BOOL                  downloadActive;
@property (nonatomic, strong) NSString              *downloadedTime;
@property (nonatomic, strong) NSString              *shortWhenAnnotation;
@property (nonatomic, assign) TBActivityLogType     logType;

- (id)initWithDictionary:(NSDictionary *)dict;

@end
