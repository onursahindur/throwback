//
//  TBNavigationController.h
//  Throwback
//
//  Created by Onur Şahindur on 01/07/16.
//  Copyright © 2016 Throwback Corp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TBNavigationController : UINavigationController

@end
