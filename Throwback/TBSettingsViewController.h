//
//  TBSettingsViewController.h
//  Throwback
//
//  Created by Onur Şahindur on 03/07/16.
//  Copyright © 2016 Throwback Corp. All rights reserved.
//

#import "TBViewController.h"

@protocol TBSettingsViewControllerDelegate <NSObject>

- (void)settingsVCLogoutButtonTapped;

@end

@interface TBSettingsViewController : UITableViewController

@property (weak, nonatomic) id<TBSettingsViewControllerDelegate>delegate;

@end
